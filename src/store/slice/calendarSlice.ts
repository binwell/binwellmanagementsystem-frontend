import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { EventLog, 
         EventLogEdit, 
         getEventLogs,
         postEventLog, 
         putEventLog, 
         deleteEventLog, 
         getEventLog, 
         mapEventLogFromServer,
         mapEventLogEditFromServer, 
         EventLogFromServer,
         EventLogEditFromServer} from "../../DAL/Calendar";

import { REJECTED, CREATED, zeroGuid } from "../../shared/Constants";
import { toUTC } from "../../shared/DateUtils";
import { AxiosResponse } from "axios";
import { ErrorObject } from "../../shared/Common";

export type EventLogsInfoType = {
    employeeId: string | null,
    status: string | null,
    fromDate: Date | null,
    toDate: Date | null
}

export type PatchEventLogInfoType = {
    id: string,
    status: string
}

export interface EventLogsState {
    loading: boolean;
    posting: boolean;
    eventLogs: EventLog[];
    current: EventLogEdit | null;
    postedResult: EventLogEdit | null;
    isAdding: boolean;
    isEditing: boolean;
    isDeleting: boolean;

    needToUpdate: boolean;
    error: ErrorObject | null;
}

const InitialEventLogsState: EventLogsState = {
    loading: false,
    posting: false,
    eventLogs: [],
    current: null,
    postedResult: null,
    isAdding: false,
    isEditing: false,
    isDeleting: false,
    needToUpdate: true,
    error: null
}


//#region AsyncThunk

// put to DAL ?????
const isValidDates = async (eventLog: EventLogEdit): Promise<boolean>=>{
    let eventLogs: EventLog [] = [];

    await getEventLogs(eventLog.employeeId, null, eventLog.startDate, eventLog.endDate || eventLog.startDate)
    .then(res => {
        eventLogs = res.data.map(mapEventLogFromServer);
        eventLogs = eventLogs.filter(ev => ev.approvalStatusId !== REJECTED);
        // Remove current eventLog 
        eventLogs = eventLogs.filter(ev => ev.id !== eventLog.id);        
    })

    if(eventLogs?.length === 0){
        return true;
    }
    else{
        return false;
    }
} 

export const getEventLogsAsyncThunk = createAsyncThunk(
    'calendar/getEventLogs',
    async (eventLogsInfo: EventLogsInfoType): Promise<EventLog[]> => {
        const { employeeId, status, fromDate, toDate } = eventLogsInfo;
        const response: AxiosResponse<EventLogFromServer[]> = await getEventLogs(employeeId, status, fromDate, toDate);
        const eventLogs = response.data.map(mapEventLogFromServer);
        return eventLogs;
    }
)

export const postEventLogAsyncThunk = createAsyncThunk(
    'calendar/postEventLog',
    async (eventLog: EventLogEdit): Promise<boolean> => {
        if(await isValidDates(eventLog)) {
            await postEventLog(eventLog);
            return true;            
        }          
        else {
            return false;
        }
    }
)

export const putEventLogAsyncThunk = createAsyncThunk(
    'calendar/putEventLog',
    async (eventLog: EventLogEdit): Promise<boolean> => {
        if(await isValidDates(eventLog)) {
            await putEventLog(eventLog);
            return true;
        }
        else {
            return false;
        }
    }
)

export const patchEventLogAsyncThunk = createAsyncThunk(
    'calendar/patchEventLog',
    async (patchEvenLogInfo: PatchEventLogInfoType): Promise<boolean> => {
        
        const response: AxiosResponse<EventLogEditFromServer> = await getEventLog(patchEvenLogInfo.id);
        let eventLog: EventLogEdit | null = null;
        eventLog = mapEventLogEditFromServer(response.data);
        eventLog.approvalStatusId = patchEvenLogInfo.status;
        eventLog.approvalDate = toUTC(new Date());
        eventLog.startDate = toUTC(eventLog.startDate);
        eventLog.endDate = eventLog.endDate ? toUTC(eventLog.endDate) : null;

        if(eventLog) {
            await putEventLog(eventLog);
            return true;
        }
        else {
            return false;
        }
    }
)

export const deleteEventLogAsyncThunk = createAsyncThunk(
    'calenda/deleteEventLog',
    async (id: string): Promise<boolean> => {
        await deleteEventLog(id);
        return true;
    }
)

export const deletingEvenLogAsyncThunk = createAsyncThunk(
    'calendar/deletingEventLog',
    async (id: string): Promise<EventLogEdit> => {
        const response: AxiosResponse<EventLogEditFromServer> = await getEventLog(id);
        const eventLog = mapEventLogEditFromServer(response.data);
        return eventLog;
    }
)

export const editingEventLogAsyncThunk = createAsyncThunk(
    'calendar/editingEventLog',
    async (id: string): Promise<EventLogEdit> => {
        const response: AxiosResponse<EventLogEditFromServer> = await getEventLog(id);
        const eventLog = mapEventLogEditFromServer(response.data);
        return eventLog;
    }
)
//#endregion

const calendarSlice = createSlice({
    name: 'calendar',
    initialState: InitialEventLogsState,
    reducers: {
        addingEventLog(state) {
            const eventLog: EventLogEdit = {
                id: zeroGuid,
                employeeId: zeroGuid,
                eventCategoryId: zeroGuid,
                reason: undefined,
                startDate: new Date(),
                endDate: null,
                approvalStatusId: CREATED,
                approvalDate: null
            };

            state.isAdding = true;
            state.current = eventLog;
        },
        clearEventLog(state) {
            state.isAdding = false;
            state.isEditing = false; 
            state.isDeleting = false;
            state.postedResult = null;
            state.current = null;
        },
        clearErrorCalendar(state) {
            state.error = null;
        }
    },
    extraReducers: builder => {
        builder 
            //#region ------------ getEventLogs -------------------------
            .addCase(getEventLogsAsyncThunk.pending, (state) => {
                state.loading = true;
                state.eventLogs = [];
            })
            .addCase(getEventLogsAsyncThunk.fulfilled, (state, action: PayloadAction<EventLog[]>) => {
                let nextEventLogs: EventLog[] = {...state.eventLogs};
                
                // action.payload.forEach(element => {
                //     if(!nextEventLogs[element.eventLogId]){
                //         nextEventLogs[element.eventLogId]=element
                //     }
                // });

                action.payload.forEach((element, index) => {
                    if(!nextEventLogs[index]) {
                        nextEventLogs[index] = element
                    }
                });

                state.eventLogs = action.payload;
                state.loading = false;
                state.needToUpdate = false;
            })
            .addCase(getEventLogsAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось получить информацию об изменениях в календаре', error: action.error.message };
                state.loading = false;
            })
            //#endregion

            //#region ------------ postEventLog -------------------------
            .addCase(postEventLogAsyncThunk.pending, (state) => {
                state.posting = true;
            })
            .addCase(postEventLogAsyncThunk.fulfilled, (state, action: PayloadAction<boolean>) => {
                if(action.payload) {
                    state.isAdding = false;
                    state.needToUpdate = true;
                    state.current = null;
                }
                else {
                    state.error = { message: 'Выберите другой период' };                    
                }

                state.posting = false;
            })
            .addCase(postEventLogAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
                state.posting = false;
            })
            //#endregion

            //#region ------------ putEventLog -------------------------
            .addCase(putEventLogAsyncThunk.pending, (state) => {
                state.posting = true;
            })
            .addCase(putEventLogAsyncThunk.fulfilled, (state, action: PayloadAction<boolean>) => {
                if(action.payload) {
                    state.isEditing = false;
                    state.needToUpdate = true;                    
                    state.current = null;
                }
                else {
                    state.error = { message: 'Выберите другой период' };
                }

                state.posting = false;
            })
            .addCase(putEventLogAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
                state.posting = false;
            })
            //#endregion

            //#region ------------ patchEventLog -------------------------
            .addCase(patchEventLogAsyncThunk.pending, (state) => {
                state.posting = true;
            })
            .addCase(patchEventLogAsyncThunk.fulfilled, (state, action: PayloadAction<boolean>) => {
                if(action.payload) {
                    state.isEditing = false;
                    state.needToUpdate = true;
                    state.current = null;
                }
                else {
                    state.error = { message: 'Не удалось получить информацию о событии' };                    
                }
                state.posting = false;
            })
            .addCase(patchEventLogAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
                state.posting = false;
            })
            //#endregion

            //#region ------------ deletingEventLog -------------------------
            .addCase(deletingEvenLogAsyncThunk.fulfilled, (state, action: PayloadAction<EventLogEdit>) => {
                state.isDeleting = true;
                state.current = action.payload;
            })
            .addCase(deletingEvenLogAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось получить информацию о событии', error: action.error.message };
            })
            //#endregion
            
            //#region ------------ deleteEventLog -------------------------
            .addCase(deleteEventLogAsyncThunk.fulfilled, (state) => {
                state.isDeleting = false;
                state.needToUpdate = true;
                state.current = null;
            })
            .addCase(deleteEventLogAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось выполнить удаление', error: action.error.message };
            })
            //#endregion

            //#region ------------ editingEventLog -------------------------
            .addCase(editingEventLogAsyncThunk.fulfilled, (state, action: PayloadAction<EventLogEdit>) => {
                state.isEditing = true;
                state.current = action.payload;
            })
            .addCase(editingEventLogAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось получить информацию о событии', error: action.error.message };
            })
            //#endregion
    }
})

export const { addingEventLog, clearEventLog, clearErrorCalendar } = calendarSlice.actions
export default calendarSlice.reducer