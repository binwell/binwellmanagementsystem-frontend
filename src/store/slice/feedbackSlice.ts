import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit"
import { sendFeedback } from "../../DAL/Feedback"
import { ErrorObject } from "../../shared/Common";

export interface FeedbackState {
    isAdding: boolean;
    result: boolean;
    posting: boolean;
    error: ErrorObject | null;
}

const InitialFeedbackState: FeedbackState = {
    isAdding: false,
    result: false,
    posting: false,
    error: null
}

export const sendFeedbackAsyncThunk = createAsyncThunk(
    'feedback/sendFeedback',
    async (message: string): Promise<boolean> => {
        await sendFeedback(message);
        return true;
    }
)

const feedbackSlice = createSlice({
    name: 'feedback',
    initialState: InitialFeedbackState,
    reducers: {
        addingFeedback(state) {
            state.isAdding = true;
        },
        clearFeedback(state) {
            state.isAdding = false;
        },
        clearErrorFeedback(state) {
            state.error = null;
        }
    },
    extraReducers: builder => {
        builder
        .addCase(sendFeedbackAsyncThunk.pending, (state) => {
            state.posting = true;
        })
        .addCase(sendFeedbackAsyncThunk.fulfilled, (state, action: PayloadAction<boolean>) => {
            state.isAdding = false;
            state.posting = false;
            state.result = action.payload;
        })
        .addCase(sendFeedbackAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось отправить отзыв', error: action.error.message };
            state.posting = false;
        })
    }
})

export const { addingFeedback, clearFeedback, clearErrorFeedback } = feedbackSlice.actions
export default feedbackSlice.reducer