import { Expense, ExpenseEdit, getExpenses, mapExpenseFromServer, uploadFile, postExpense, putExpense, getExpense, mapExpenseEditFromServer, deleteExpense, saveFile, Currencies, PaymentMethods, ExpenseFromServer, ExpenseEditFromServer, putExpenseChangeStatus } from "../../DAL/Expenses";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { getUserId } from "../../shared/LocalStorageUtils";
import { toUTC } from "../../shared/DateUtils";
import save from "save-file";
import { CREATED, zeroGuid } from "../../shared/Constants";
import { AxiosResponse } from "axios";
import { ErrorObject } from "../../shared/Common";

export interface ExpensesState {
    loading: boolean;
    posting: boolean;
    expenses: Expense[];
    current: ExpenseEdit | null;
    postedResult: ExpenseEdit | null;
    isAdding: boolean;
    isEditing: boolean;
    isDeleting: boolean;
    documentId: number | null;

    needToUpdate: boolean;
    loadingDocument: boolean;
    error: ErrorObject | null;
}

const InitialExpensesState: ExpensesState = {
    loading: false,
    posting: false,
    expenses: [],
    current: null,
    postedResult: null,
    isAdding: false,
    isEditing: false,
    isDeleting: false,
    documentId: null,
    needToUpdate: true,
    loadingDocument: false,
    error: null
}

export type ExpensesInfoType = {
    employeeId: string | null,
    status: string | null,
    fromDate: Date | null,
    toDate: Date | null
}

export type ExpenseInfoType = {
    expense: ExpenseEdit,
    file: FormData | null
}

export type PatchExpenseInfoType = {
    id: string,
    status: string
}

export type DocumentInfoType = {
    id: number,
    fileName: string
}

//#region --------------------- AsyncThunk --------------------------

export const getExpensesAsyncThunk = createAsyncThunk(
    'expenses/getExpenses',
    async (expensesInfo: ExpensesInfoType): Promise<Expense[]> => {
        const { employeeId, status, fromDate, toDate } = expensesInfo;
        const response: AxiosResponse<ExpenseFromServer[]> = await getExpenses( employeeId, status, fromDate, toDate);
        const expenses: Expense[] = response.data.map(mapExpenseFromServer);
        return expenses;
    }
)

export const postExpenseAsyncThunk = createAsyncThunk(
    'expenses/postExpense',
    async (postExpenseInfo: ExpenseInfoType): Promise<boolean> => {
        if(postExpenseInfo.file) {
            const response: AxiosResponse<number> = await uploadFile(postExpenseInfo.file);
            postExpenseInfo.expense.documentId = response.data;
        }

        await postExpense(postExpenseInfo.expense);
        return true;
    }
)

export const putExpenseAsyncThunk = createAsyncThunk(
    'expenses/putExpense',
    async (putExpenseInfo: ExpenseInfoType): Promise<boolean> => {
        if(putExpenseInfo.file) {
            const response: AxiosResponse<number> = await uploadFile(putExpenseInfo.file);
            putExpenseInfo.expense.documentId = response.data;
        }

        await putExpense(putExpenseInfo.expense);
        return true;
    }
)

export const patchExpenseAsyncThunk = createAsyncThunk(
    'expenses/patchExpense',
    async(patchExpenseInfo: PatchExpenseInfoType): Promise<boolean> => {
        await putExpenseChangeStatus(patchExpenseInfo.id, patchExpenseInfo.status);
        return true;
    }
)

export const deleteExpenseAsyncThunk = createAsyncThunk(
    'expenses/deleteExpense',
    async (id: string): Promise<boolean> => {
        await deleteExpense(id);
        return true;
    }
)

export const saveDocumentAsyncThunk = createAsyncThunk(
    'expenses/saveDocument',
    async (documentInfo: DocumentInfoType): Promise<boolean> => {
        const response = await saveFile(documentInfo.id);
        await save(new Blob([response.data]), documentInfo.fileName);
        return true;
    }
)

export const deletingExpenseAsyncThunk = createAsyncThunk(
    'expenses/deletingExpense',
    async (id: string): Promise<ExpenseEdit> => {
        const response: AxiosResponse<ExpenseEditFromServer> = await getExpense(id);
        const expense = mapExpenseEditFromServer(response.data);
        return expense;
    }
)

export const editingExpenseAsyncThunk = createAsyncThunk(
    'expenses/editingExpense',
    async (id: string): Promise<ExpenseEdit> => {
        const response: AxiosResponse<ExpenseEditFromServer> = await getExpense(id);
        const expense = mapExpenseEditFromServer(response.data);
        return expense;
    }
)

//#endregion

const expensesSlice = createSlice({
    name: 'expenses',
    initialState: InitialExpensesState,
    reducers: {
        addingExpense(state) {
            const expense: ExpenseEdit | null = {
                id: zeroGuid,
                employeeId: zeroGuid,
                expenseCategoryId: zeroGuid,
                documentId: null,
                description: undefined,
                amount: 0,
                transactionDate: new Date(),
                paymentDate: null,
                approvalStatusId: CREATED,
                currencyId: Currencies[0].currencyId,
                paymentMethodId: PaymentMethods[0].paymentMethodId,
                managerId: null // need to fix
            };

            state.isAdding = true;
            state.current = expense;
        },
        clearExpense(state) {
            state.isAdding = false;
            state.isEditing = false; 
            state.isDeleting = false;
            state.postedResult = null;
            state.current = null;
        },
        clearErrorExpenses(state) {
            state.error = null;
        }
    },
    extraReducers: builder => {
        builder

        //#region ------------- getExpenses ----------------------
        .addCase(getExpensesAsyncThunk.pending, (state) => {
            state.loading = true;
        })
        .addCase(getExpensesAsyncThunk.fulfilled, (state, action: PayloadAction<Expense[]>) => {
            let nextExpenses: Expense[] = Object.assign({}, state.expenses);
            // action.payload.forEach(element => {
            //     if(!nextExpenses[element.expenseId]){
            //         nextExpenses[element.expenseId]=element
            //     }
            // });
            
            action.payload.forEach((element, index) => {
                if(!nextExpenses[index]) {
                    nextExpenses[index] = element
                }
            });

            state.expenses = action.payload;
            state.needToUpdate = false;
            state.loading = false;
        })
        .addCase(getExpensesAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить список расходов', error: action.error.message };
            state.loading = false;
        })
        //#endregion

        //#region ------------- postExpense ----------------------
        .addCase(postExpenseAsyncThunk.pending, (state) => {
            state.posting = true;
        })
        .addCase(postExpenseAsyncThunk.fulfilled, (state) => {
            state.isAdding = false;
            state.needToUpdate = true;
            state.posting = false;
            state.current = null;
        })
        .addCase(postExpenseAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
            state.posting = false;
        })
        //#endregion

        //#region ------------- putExpense ----------------------
        .addCase(putExpenseAsyncThunk.pending, (state) => {
            state.posting = true;
        })
        .addCase(putExpenseAsyncThunk.fulfilled, (state) => {
            state.isAdding = false;
            state.needToUpdate = true;
            state.posting = false;
            state.current = null;
        })
        .addCase(putExpenseAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
            state.posting = false;
        })
        //#endregion

        //#region ------------- patchExpense ----------------------
        .addCase(patchExpenseAsyncThunk.fulfilled, (state, action) => {
            if(action.payload) {
                state.isEditing = false;
                state.needToUpdate = true;
                state.current = null; 
            }
            else {
                state.error = { message: 'Не удалось получить информацию о расходе' };
            }
            
            state.posting = false;
        })
        .addCase(patchExpenseAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
            state.posting = false;
        })
        //#endregion

        //#region ------------- saveDocument ----------------------
        .addCase(saveDocumentAsyncThunk.pending, (state) => {
            state.loadingDocument = true;
        })
        .addCase(saveDocumentAsyncThunk.fulfilled, (state) => {
            state.loadingDocument = false;
        })
        .addCase(saveDocumentAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить документ', error: action.error.message };
            state.loadingDocument = false;
        })
        //#endregion

        //#region ------------- deleteExpense ----------------------
        .addCase(deleteExpenseAsyncThunk.fulfilled, (state) => {
            state.isDeleting = false;
            state.needToUpdate = true;
            state.current = null;
        })
        .addCase(deleteExpenseAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось выполнить удаление', error: action.error.message };
        })
        //#endregion

        //#region ------------- deletingExpense ----------------------
        .addCase(deletingExpenseAsyncThunk.fulfilled, (state, action: PayloadAction<ExpenseEdit>) => {
            state.isDeleting = true;
            state.current = action.payload;
        })
        .addCase(deletingExpenseAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить информацию о расходе', error: action.error.message };
        })
        //#endregion

        //#region ------------- editingExpense ----------------------
        .addCase(editingExpenseAsyncThunk.fulfilled, (state, action: PayloadAction<ExpenseEdit>) => {
            state.isEditing = true;
            state.current = action.payload;
        })
        .addCase(editingExpenseAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить информацию о расходе', error: action.error.message };
        })
        //#endregion

    }
})

export const { addingExpense, clearExpense, clearErrorExpenses } = expensesSlice.actions
export default expensesSlice.reducer