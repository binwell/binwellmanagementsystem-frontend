import { Employee, EmployeeEdit, getEmployees, mapEmployeeFromServer, getEmployee, mapEmployeeEditFromServer, postEmployee, putEmployee, deleteEmployee, Role, EmploymentTypes, EmployeeFromServer, EmployeeEditFromServer } from "../../DAL/Employees";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { toUTC } from "../../shared/DateUtils";
import { EMPLOYEE, zeroGuid } from "../../shared/Constants";
import { AxiosResponse } from "axios";
import { ErrorObject } from "../../shared/Common";

export interface EmployeesState {
    loading: boolean;
    posting: boolean;
    employees: Employee[];
    current: EmployeeEdit | null;
    postedResult: EmployeeEdit | null;
    isAdding: boolean;
    isEditing: boolean;
    isDeleting: boolean;

    needToUpdate: boolean;
    error: ErrorObject | null;
}

const InitialEmployeesState: EmployeesState = {
    loading: false,
    posting: false,
    employees: [],
    current: null,
    postedResult: null,
    isAdding: false,
    isEditing: false,
    isDeleting: false,
    needToUpdate: true,
    error: null
}

export type PatchEmployeeInfoType = {
    id: string,
    roles: Role[]
}

//#region ------------- AsynkThunk --------------------
export const getEmployeesAsyncThunk = createAsyncThunk(
    'employees/getEmployees',
    async (includeFired: boolean = false): Promise<Employee[]> => {
        const response: AxiosResponse<EmployeeFromServer[]>  = await getEmployees(includeFired);
        const employees = response.data.map(mapEmployeeFromServer);
        return employees;
    }
)

export const getEmployeeAsyncThunk = createAsyncThunk(
    'employees/getEmployee',
    async (id: string): Promise<EmployeeEdit> => {
        const response: AxiosResponse<EmployeeEditFromServer> = await getEmployee(id);
        const employee = mapEmployeeEditFromServer(response.data);
        return employee;
    }
)

export const postEmployeeAsyncThunk = createAsyncThunk(
    'employees/postEmployee',
    async (employee: EmployeeEdit): Promise<boolean> => {
        await postEmployee(employee);
        return true;
    }
)

export const putEmployeeAsyncThunk = createAsyncThunk(
    'employees/putEmployee',
    async (employee: EmployeeEdit): Promise<boolean> => {
        await putEmployee(employee);
        return true;
    }
)

export const deleteEmployeeAsyncThunk = createAsyncThunk(
    'employees/deleteEmployee',
    async (id: string): Promise<boolean> => {
        await deleteEmployee(id);
        return true;
    }
)

export const patchEmployeeAsyncThunk = createAsyncThunk(
    'employees/patchEmployee',
    async (patchEmployeeInfo: PatchEmployeeInfoType): Promise<boolean> => {
        const response: AxiosResponse<EmployeeEditFromServer> = await getEmployee(patchEmployeeInfo.id);
        let employee: EmployeeEdit | null = null;
        employee = mapEmployeeEditFromServer(response.data);
        employee.roles = patchEmployeeInfo.roles;
        employee.birthDate = toUTC(employee.birthDate);
        employee.employedDate = toUTC(employee.employedDate);
        employee.leaveDate = employee.leaveDate ? toUTC(employee.leaveDate) : null;

        if(employee) {
            await putEmployee(employee);
            return true;
        }
        else {
            return false;
        }
    }
)

export const deletingEmployeeAsyncThunk = createAsyncThunk(
    'employees/deletingEmployee',
    async (id: string): Promise<EmployeeEdit> => {
        const response: AxiosResponse<EmployeeEditFromServer> = await getEmployee(id);
        const employee = mapEmployeeEditFromServer(response.data);
        return employee;
    }
)

export const editingEmployeeAsyncThunk = createAsyncThunk(
    'employees/editingEmployee',
    async (id: string): Promise<EmployeeEdit> => {
        const response: AxiosResponse<EmployeeEditFromServer> = await getEmployee(id);
        const employee = mapEmployeeEditFromServer(response.data);
        return employee;
    }
)
//#endregion

const employeesSlice = createSlice({
    name: 'employees',
    initialState: InitialEmployeesState,
    reducers: {
        addingEmployee(state) {
            let employee: EmployeeEdit | null = {
                id: zeroGuid,
                fullName: undefined,
                email: undefined,
                phone: undefined,
                birthDate: new Date(),
                employedDate: new Date(),
                leaveDate: null,
                managerId: zeroGuid,
                roles: [{id: EMPLOYEE, title: "Сотрудник"}],
                employmentTypeId: EmploymentTypes[0].employmentTypeId
            };

            state.isAdding = true;
            state.current = employee;
        },
        clearEmployee(state) {
            state.isAdding = false;
            state.isEditing = false; 
            state.isDeleting = false;
            state.postedResult = null;
            state.current = null;
        },
        clearErrorEmployees(state) {
            state.error = null;
        }
    },
    extraReducers: builder => {
        builder
            //#region -------------- getEmployees -----------------------
            .addCase(getEmployeesAsyncThunk.pending, (state) => {
                state.loading = true;
                state.employees = [];
            })
            .addCase(getEmployeesAsyncThunk.fulfilled, (state, action: PayloadAction<Employee[]>) => {
                state.employees = action.payload;
                state.needToUpdate = false;
                state.loading = false;
            })
            .addCase(getEmployeesAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не уалось получить список сотрудников', error: action.error.message };
                state.loading = false;
            })
            //#endregion

            //#region -------------- getEmployee -----------------------
            .addCase(getEmployeeAsyncThunk.pending, (state) => {
                state.current = null;
            })
            .addCase(getEmployeeAsyncThunk.fulfilled, (state, action: PayloadAction<EmployeeEdit>) => {
                state.current = action.payload;
            })
            .addCase(getEmployeeAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось получить информацию о сотруднике', error: action.error.message };
            })
            //#endregion

            //#region -------------- postEmployee -----------------------
            .addCase(postEmployeeAsyncThunk.pending, (state) => {
                state.posting = true;
            })
            .addCase(postEmployeeAsyncThunk.fulfilled, (state) => {
                state.isAdding = false;
                state.needToUpdate = true;
                state.posting = false;
                state.current = null;
            })
            .addCase(postEmployeeAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
                state.posting = false;                
            })
            //#endregion

            //#region -------------- putEmployee -----------------------
            .addCase(putEmployeeAsyncThunk.pending, (state) => {
                state.posting = true;
            })
            .addCase(putEmployeeAsyncThunk.fulfilled, (state) => {
                state.isEditing = false;
                state.needToUpdate = true;
                state.posting = false;
                state.current = null;
            })
            .addCase(putEmployeeAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
                state.posting = false; 
            })
            //#endregion

            //#region -------------- patchEmployee -----------------------
            .addCase(patchEmployeeAsyncThunk.pending, (state) => {
                state.posting = true;
            })
            .addCase(patchEmployeeAsyncThunk.fulfilled, (state, action: PayloadAction<boolean>) => {
                if(action.payload) {
                    state.isEditing = false;
                    state.needToUpdate = true;                    
                    state.current = null;
                }
                else {
                    state.error = { message: 'Не удалось получить информацию о сотруднике' }
                }

                state.posting = false;
            })
            .addCase(patchEmployeeAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось сохранить изменения', error: action.error.message }
            })
            //#endregion

            //#region -------------- deleteEmployee -----------------------
            .addCase(deleteEmployeeAsyncThunk.fulfilled, (state) => {
                state.isDeleting = false;
                state.needToUpdate = true;
                state.current = null;
            })
            .addCase(deleteEmployeeAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось выполнить удаление', error: action.error.message };
            })
            //#endregion

            //#region -------------- deletingEmployee -----------------------
            .addCase(deletingEmployeeAsyncThunk.fulfilled, (state, action: PayloadAction<EmployeeEdit>) => {
                state.isDeleting = true;
                state.current = action.payload;
            })
            .addCase(deletingEmployeeAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось получить информацию о сотруднике', error: action.error.message };
            })
            //#endregion

            //#region -------------- editingEmployee -----------------------
            .addCase(editingEmployeeAsyncThunk.fulfilled, (state, action: PayloadAction<EmployeeEdit>) => {
                state.isEditing = true;
                state.current = action.payload;
            })
            .addCase(editingEmployeeAsyncThunk.rejected, (state, action) => {
                state.error = { message: 'Не удалось получить информацию о сотруднике', error: action.error.message };
            })
            //#endregion
    }
})

export const { addingEmployee, clearEmployee, clearErrorEmployees } = employeesSlice.actions
export default employeesSlice.reducer