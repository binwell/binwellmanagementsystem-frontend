import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { EventLogCategory, getEventLogCategories, postEventLogCategory, putEventLogCategory, deleteEventLogCategory, getEventLogCategory } from "../../DAL/Dictionaries";
import { AxiosResponse } from "axios";
import { ErrorObject } from "../../shared/Common";
import { zeroGuid } from "../../shared/Constants";

export interface EventLogCategoriesState{
    loading: boolean;
    posting: boolean;
    eventLogCategories: EventLogCategory[];    
    current: EventLogCategory | null;
    postedResult: EventLogCategory | null;
    isAdding: boolean;
    isEditing: boolean;
    isDeleting: boolean;

    needToUpdate: boolean;
    error: ErrorObject | null;
}

const InitialEventLogCategoriesState: EventLogCategoriesState = {
    loading: false,
    posting: false,
    eventLogCategories: [],
    current: null,
    postedResult: null,
    isAdding: false,
    isEditing: false,
    isDeleting: false,
    needToUpdate: true,
    error: null,
}

//#region -------------- AsyncThunk ----------------------------
export const getEventLogCategoriesAsyncThunk = createAsyncThunk(
    'eventLogCategories/getEventLogCategories',
    async (): Promise<EventLogCategory[]> => {
        const response: AxiosResponse<EventLogCategory[]> = await getEventLogCategories();
        return response.data;
    }
)

export const postEventLogCategoryAsyncThunk = createAsyncThunk(
    'eventLogCategories/postEventLogCategory',
    async (eventLogCategory: EventLogCategory): Promise<boolean> => {
        await postEventLogCategory(eventLogCategory);
        return true;
    }
)

export const putEventLogCategoryAsyncThunk = createAsyncThunk(
    'eventLogCategories/putEventLogCategory',
    async (eventLogCategory: EventLogCategory): Promise<boolean> => {
        await putEventLogCategory(eventLogCategory);
        return true;
    }
)

export const deleteEventLogCategoryAsyncThunk = createAsyncThunk(
    'eventLogCategories/deleteEventLogCategory',
    async (id: string): Promise<boolean> => {
        await deleteEventLogCategory(id);
        return true;
    }
)

export const deletingEventLogCategoryAsyncThunk = createAsyncThunk(
    'eventLogCategories/deletingEventLogCategory',
    async (id: string): Promise<EventLogCategory> => {
        const response: AxiosResponse<EventLogCategory> = await getEventLogCategory(id);
        return response.data;
    }
)

export const editingEventLogCategoryAsyncThunk = createAsyncThunk(
    'eventLogCategories/editingEventLogCategory',
    async (id: string): Promise<EventLogCategory> => {
        const response: AxiosResponse<EventLogCategory> = await getEventLogCategory(id);
        return response.data;
    }
)
//#endregion

const eventLogCategoriesSlice = createSlice({
    name: 'eventLogCategories',
    initialState: InitialEventLogCategoriesState,
    reducers: {
        addingEventLogCategory(state) {
            const eventLogCategory: EventLogCategory = {
                id: zeroGuid,
                title: undefined,
                color: undefined,
                limit: 0
            };

            state.isAdding = true;
            state.current = eventLogCategory;
        },
        clearEventLogCategory(state) {
            state.isEditing = false;
            state.isDeleting = false;
            state.current = null;
            state.postedResult = null;
        },
        clearErrorEventLogCategories(state) {
            state.error = null;
        }
    },
    extraReducers: builder => {
        builder
        
        //#region ---------------- getEventLogCategories --------------------
        .addCase(getEventLogCategoriesAsyncThunk.pending, (state) => {
            state.eventLogCategories = [];
            state.loading = true;
        })
        .addCase(getEventLogCategoriesAsyncThunk.fulfilled, (state, action: PayloadAction<EventLogCategory[]>) => {
            state.eventLogCategories = action.payload;
            state.loading = false;
            state.needToUpdate = false;
        })
        .addCase(getEventLogCategoriesAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить список категорий изменений в каледнаре', error: action.error.message };
            state.loading = false;
        })
        //#endregion

        //#region ---------------- postEventLogCategory --------------------
        .addCase(postEventLogCategoryAsyncThunk.pending, (state) => {
            state.posting = true;
        })
        .addCase(postEventLogCategoryAsyncThunk.fulfilled, (state) => {
            state.postedResult = null;
            state.current = null;
            state.isAdding = false;
            state.needToUpdate = true;
            state.posting = false;
        })
        .addCase(postEventLogCategoryAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
            state.posting = false;
        })
        //#endregion

        //#region ---------------- putEventLogCategory --------------------
        .addCase(putEventLogCategoryAsyncThunk.pending, (state) => {
            state.posting = true;
        })
        .addCase(putEventLogCategoryAsyncThunk.fulfilled, (state) => {
            state.isEditing = false;
            state.needToUpdate = true;
            state.posting = false;
            state.current = null;
        })
        .addCase(putEventLogCategoryAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
            state.posting = false;
        })
        //#endregion

        //#region ---------------- deleteEventLogCategory --------------------
        .addCase(deleteEventLogCategoryAsyncThunk.fulfilled, (state) => {
            state.isDeleting = false;
            state.needToUpdate = true;
            state.current = null;
        })
        .addCase(deleteEventLogCategoryAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось выполнить удаление', error: action.error.message };
        })
        //#endregion

        //#region ---------------- deletingEventLogCategory --------------------
        .addCase(deletingEventLogCategoryAsyncThunk.fulfilled, (state, action: PayloadAction<EventLogCategory>) => {
            state.current = action.payload;
            state.isDeleting = true;
        })
        .addCase(deletingEventLogCategoryAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить информацию о категории', error: action.error.message };
        })
        //#endregion

        //#region ---------------- editingEventLogCategory --------------------
        .addCase(editingEventLogCategoryAsyncThunk.fulfilled, (state, action: PayloadAction<EventLogCategory>) => {
            state.isEditing = true;
            state.current = action.payload;
        })
        .addCase(editingEventLogCategoryAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить информацию о категории', error: action.error.message };
        })
        //#endregion
    }
})

export const { addingEventLogCategory, clearEventLogCategory, clearErrorEventLogCategories } = eventLogCategoriesSlice.actions
export default eventLogCategoriesSlice.reducer