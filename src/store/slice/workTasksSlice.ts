import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { WorkTask, getWorkTasks, postWorkTask, putWorkTask, getWorkTask } from "../../DAL/TimeTracking";
import { AxiosResponse } from "axios";
import { ErrorObject } from "../../shared/Common";
import { zeroGuid } from "../../shared/Constants";

export interface WorkTasksState {
    loading: boolean;
    posting: boolean;
    workTasks: WorkTask[];
    current: WorkTask | null;
    postedResult: WorkTask | null;
    isAdding: boolean;
    isEditing: boolean;
    needToUpdate: boolean;
    error: ErrorObject | null;
}


const InitialWorkTasksState: WorkTasksState = {
    loading: false,
    posting: false,
    workTasks: [],
    current: null,
    postedResult: null,
    isAdding: false,
    isEditing: false,
    needToUpdate: false,
    error: null
}

//#region ----------------- AsyncThunk --------------------------
export const getWorkTasksAsyncThunk = createAsyncThunk(
    'workTasks/getWorkTasks',
    async (employeeId: string | null = null): Promise<WorkTask[]> => {
        const response: AxiosResponse<WorkTask[]> = await getWorkTasks(employeeId);
        const workTasks = response.data;
        return workTasks;
    }
)

export const postWorkTaskAsyncThunk = createAsyncThunk(
    'workTasks/postWorkTask',
    async (workTask: WorkTask): Promise<boolean> => {
        await postWorkTask(workTask);
        return true;
    }
)

export const putWorkTaskAsyncThynk = createAsyncThunk(
    'workTasks/putWorkTask',
    async (workTask: WorkTask): Promise<boolean> => {
        await putWorkTask(workTask);
        return true;
    }
)

export const editingWorkTaskAsyncThunk = createAsyncThunk(
    'workTasks/editingWorkTask',
    async (id: string): Promise<WorkTask> => {
        const response: AxiosResponse<WorkTask> = await getWorkTask(id);
        const workTask = response.data;
        return workTask;
    }
)

//#endregion

const workTasksSlice = createSlice({
    name: 'workTasks',
    initialState: InitialWorkTasksState,
    reducers: {
        addingWorkTask(state){
            const workTask: WorkTask | null = {
                id: zeroGuid,
                projectId: '',
                title: undefined,
                employeeId: '',
            };

            state.isAdding = true;
            state.current = workTask;
        },
        clearWorkTask(state){
            state.isAdding = false;
            state.isEditing = false;
            state.postedResult = null;
            state.current = null;
        },
        clearErrorWorkTasks(state) {
            state.error = null;
        }
    },
    extraReducers: builder => {
        builder
        //#region ------------------ getWorkTasks -----------------------
        .addCase(getWorkTasksAsyncThunk.pending, (state) => {
            state.loading = true;
            state.workTasks = [];
        })
        .addCase(getWorkTasksAsyncThunk.fulfilled, (state, action: PayloadAction<WorkTask[]>) => {
            state.workTasks = action.payload;
            state.loading = false;
            state.needToUpdate = false;
        })
        .addCase(getWorkTasksAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить список задач', error: action.error.message };
            state.loading = false;
        })
        //#endregion

        //#region ------------------ postWorkTask -----------------------
        .addCase(postWorkTaskAsyncThunk.pending, (state) => {
            state.posting = true;
        })
        .addCase(postWorkTaskAsyncThunk.fulfilled, (state) => {
            state.postedResult = null;
            state.current = null;
            state.isAdding = false;
            state.needToUpdate = true;
            state.posting = false;
        })
        .addCase(postWorkTaskAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
            state.posting = false;
        })
        //#endregion

        //#region ------------------ putWorkTask -----------------------
        .addCase(putWorkTaskAsyncThynk.pending, (state) => {
            state.posting = true;
        })
        .addCase(putWorkTaskAsyncThynk.fulfilled, (state) => {
            state.isEditing = false;
            state.needToUpdate = true;
            state.posting = false;
            state.current = null;
        })
        .addCase(putWorkTaskAsyncThynk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
            state.posting = false;
        })
        //#endregion

        //#region ------------------ editingWorkTask -----------------------
        .addCase(editingWorkTaskAsyncThunk.fulfilled, (state, action: PayloadAction<WorkTask>) => {
            state.isEditing = true;
            state.current = action.payload;
        })
        .addCase(editingWorkTaskAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить инфромацию о задаче', error: action.error.message };
        })
        //#endregion
    }
})

export const { addingWorkTask, clearWorkTask, clearErrorWorkTasks } = workTasksSlice.actions
export default workTasksSlice.reducer