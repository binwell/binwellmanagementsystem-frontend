import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { TimeTracking, getTimeTrackings, mapTimeTrackingFromServer, TimeTrackingEdit, postTimeTracking, putTimeTracking, deleteTimeTracking, getTimeTracking, mapTimeTrackingEditFromServer, TimeTrackingFromServer, TimeTrackingEditFromServer } from "../../DAL/TimeTracking";
import { AxiosResponse } from "axios";
import { ErrorObject } from "../../shared/Common";
import { zeroGuid } from "../../shared/Constants";

export interface TimeTrackingsState {
    loading: boolean;
    posting: boolean;
    timeTrackings: TimeTracking[];
    current: TimeTrackingEdit | null;
    postedResult: TimeTrackingEdit | null;
    isAdding: boolean;
    isEditing: boolean;
    needToUpdate: boolean;
    error: ErrorObject | null;
}

const InitialTimeTrackingsState: TimeTrackingsState = {
    loading: false,
    posting: false,
    timeTrackings: [],
    current: null,
    postedResult: null,
    isAdding: false,
    isEditing: false,
    needToUpdate: false,
    error: null
}

export type GetTimeTrackingsInfoType = {
    projectId: string | null, 
    employeeId: string | null, 
    workTaskId: string | null, 
    fromDate: Date | null, 
    toDate: Date | null
}

type AddingTimeTrackingsInfoType = {
    newWorkTaskId?: string | null, 
    newStartDate?: Date | null
}

const isValidDates = async (timeTracking: TimeTrackingEdit): Promise<boolean> => {
    // Get timeTrackings by worktask and period
    let timeTrackings: TimeTrackingEdit[] = [];

    await getTimeTrackings(null, null, timeTracking.workTaskId, timeTracking.startDate, timeTracking.startDate)
    .then(res=>{
        timeTrackings = res.data.map(mapTimeTrackingFromServer);
        // Remove current timeTracking
        timeTrackings = timeTrackings?.filter(t => t.id !== timeTracking.id);
    })    

    if(timeTrackings.length === 0){
        return true
    }
    else{
        return false
    }   
}

//#region ---------------- AsuncThunk --------------------------------
export const getTimeTrackingsAsyncThunk = createAsyncThunk(
    'timeTrackings/getTimeTrackings',
    async (timeTrackingsInfo: GetTimeTrackingsInfoType): Promise<TimeTracking[]> => {
        const { projectId, employeeId, workTaskId, fromDate, toDate } = timeTrackingsInfo;
        const response: AxiosResponse<TimeTrackingFromServer[]> = await getTimeTrackings(projectId, employeeId, workTaskId, fromDate, toDate);
        const timeTrackings = response.data.map(mapTimeTrackingFromServer);
        return timeTrackings;
    }
)

export const postTimeTrackingAsyncThunk = createAsyncThunk(
    'timeTrackings/postTimeTracking',
    async (timeTracking: TimeTrackingEdit): Promise<boolean> => {
        if(isValidDates(timeTracking)) {
            await postTimeTracking(timeTracking);
            return true;
        }
        else {
            return false;
        }
    }
)

export const putTimeTrackingAsyncThunk = createAsyncThunk(
    'timeTrackings/putTimeTracking',
    async (timeTracking: TimeTrackingEdit): Promise<boolean> => {
        if(isValidDates(timeTracking)) {
            await putTimeTracking(timeTracking);
            return true;
        }
        else {
            return false;
        }
    }
)

export const deleteTimeTrackingAsyncThunk = createAsyncThunk(
    'timeTrackings/deleteTimeTracking',
    async (id: string): Promise<boolean> => {
        await deleteTimeTracking(id);
        return true;
    }
)

export const editingTimeTrackingAsyncThunk = createAsyncThunk(
    'timeTrackings/editingTimeTracking',
    async (id: string): Promise<TimeTrackingEdit> => {
        const response: AxiosResponse<TimeTrackingEditFromServer> = await getTimeTracking(id);
        const timeTracking = mapTimeTrackingEditFromServer(response.data);
        return timeTracking;
    }
)
//#endregion

const timeTracking = createSlice({
    name: 'timeTrackings',
    initialState: InitialTimeTrackingsState,
    reducers: {
        addingTimeTracking: {
            reducer(state, action: PayloadAction<AddingTimeTrackingsInfoType>) {
                const timeTracking: TimeTrackingEdit | null = {
                    id: zeroGuid,
                    startDate: action.payload.newStartDate || new Date(),
                    timeSpent: 0,
                    workTaskId: action.payload.newWorkTaskId || ''
                };

                state.isAdding = true;
                state.current = timeTracking;
            },
            prepare(newWorkTaskId?: string | null, newStartDate?: Date | null) {
                return {
                    payload: { newWorkTaskId, newStartDate }
                }
            }
        },
        clearTimeTracking(state) {
            state.isAdding = false;
            state.isEditing = false;
            state.postedResult = null;
            state.current = null;
        },
        clearErrorTimeTrackings(state) {
            state.error = null;
        }
    },
    extraReducers: builder => {
        builder

        //#region ------------------ getTimeTrackings -----------------------
        .addCase(getTimeTrackingsAsyncThunk.pending, (state) => {
            state.timeTrackings = [];
            state.loading = true;
        })
        .addCase(getTimeTrackingsAsyncThunk.fulfilled, (state, action: PayloadAction<TimeTracking[]>) => {
            state.timeTrackings = action.payload;
            state.loading = false;
            state.needToUpdate = false;
        })
        .addCase(getTimeTrackingsAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить список учтенных часов', error: action.error.message };
            state.loading = false;
        })
        //#endregion

        //#region ------------------ postTimeTracking -----------------------
        .addCase(postTimeTrackingAsyncThunk.pending, (state) => {
            state.posting = true;
        })
        .addCase(postTimeTrackingAsyncThunk.fulfilled, (state, action: PayloadAction<boolean>) => {
            if(action.payload) {
                state.isAdding = false;
                state.needToUpdate = true;                
                state.current = null;
            }
            else {
                state.error = { message: 'Часы для этой задачи уже учтены', error: '409' };
            }

            state.posting = false;
        })
        .addCase(postTimeTrackingAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
            state.posting = false;
        })
        //#endregion

        //#region ------------------ putTimeTracking -----------------------
        .addCase(putTimeTrackingAsyncThunk.pending, (state) => {
            state.posting = true;
        })
        .addCase(putTimeTrackingAsyncThunk.fulfilled, (state, action: PayloadAction<boolean>) => {
            if(action.payload) {
                state.isEditing = false;
                state.needToUpdate = true;
                state.current = null;
            }
            else {
                state.error = { message: 'Часы для этой задачи уже учтены', error: '409' };
            }
            state.posting = false;
        })
        .addCase(putTimeTrackingAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
        })
        //#endregion

        //#region ------------------ deleteTimeTracking -----------------------
        .addCase(deleteTimeTrackingAsyncThunk.fulfilled, (state) => {
            state.current = null;
            state.needToUpdate = true;
        })
        .addCase(deleteTimeTrackingAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось выполнить удаление', error: action.error.message };
        })
        //#endregion

        //#region ------------------ editingTimeTracking -----------------------
        .addCase(editingTimeTrackingAsyncThunk.fulfilled, (state, action: PayloadAction<TimeTrackingEdit>) => {
            state.isEditing = true;
            state.current = action.payload;
        })
        .addCase(editingTimeTrackingAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить информацию об учтенных часах', error: action.error.message };
        })
        //#endregion


    }
})

export const { addingTimeTracking, clearTimeTracking, clearErrorTimeTrackings } = timeTracking.actions
export default timeTracking.reducer