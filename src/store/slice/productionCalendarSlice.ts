import { IProductionCalendar, getProductionCalendar, mapProductionCalendarFromServer } from "../../DAL/ProductionCalendar";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ErrorObject } from "../../shared/Common";


export interface ProductionCalendarState {
    loading: boolean;
    posting: boolean;

    isAdding: boolean;
    isEditing: boolean;

    needToUpdate: boolean;

    productionCalendar: IProductionCalendar[];

    error: ErrorObject | null;
}

const InitialProductionCalendarState: ProductionCalendarState = {
    loading: false,
    posting: false,
    isAdding: false,
    isEditing: false,
    needToUpdate: false,
    productionCalendar: [],
    error: null
}

export const getProductionCalendarAsyncThunk = createAsyncThunk(
    'productionCalendar/getProductionCalendar',
    async (): Promise<IProductionCalendar[]> => {
        const response = await getProductionCalendar();
        const productionCalendar = response.map(mapProductionCalendarFromServer);
        return productionCalendar;
    }
)

const productionCalendarSlice = createSlice({
    name: 'productionCalendar',
    initialState: InitialProductionCalendarState,
    reducers: {
        clearErrorProductionCalendar(state) {
            state.error = null;
        }
    },
    extraReducers: builder => {
        builder
        .addCase(getProductionCalendarAsyncThunk.pending, (state) => {
            state.loading = true;
        })
        .addCase(getProductionCalendarAsyncThunk.fulfilled, (state, action: PayloadAction<IProductionCalendar[]>) => {
            state.productionCalendar = action.payload;
            state.loading = false;
        })
        .addCase(getProductionCalendarAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить производственный календарь', error: action.error.message };
            state.loading = false;
        })
    }
})

export const { clearErrorProductionCalendar } = productionCalendarSlice.actions
export default productionCalendarSlice.reducer