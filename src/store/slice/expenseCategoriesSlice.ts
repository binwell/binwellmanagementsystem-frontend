import { ExpenseCategory, getExpenseCategories, postExpenseCategory, putExpenseCategory, deleteExpenseCategory, getExpenseCategory } from "../../DAL/Dictionaries";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AxiosResponse } from "axios";
import { ErrorObject } from "../../shared/Common";
import { zeroGuid } from "../../shared/Constants";

export interface ExpenseCategoriesState{
    loading: boolean;
    posting: boolean;
    expenseCategories: ExpenseCategory[];    
    current: ExpenseCategory | null;
    postedResult: ExpenseCategory | null;
    isAdding: boolean;
    isEditing: boolean;
    isDeleting: boolean;
    needToUpdate: boolean;
    error: ErrorObject | null;
}

const InitialExpenseCategoriesState: ExpenseCategoriesState = {
    loading: false,
    posting: false,
    expenseCategories: [],
    current: null,
    postedResult: null,
    isAdding: false,
    isEditing: false,
    isDeleting: false,
    needToUpdate: true,
    error: null
}

//#region -------------- AsyncThunk ----------------------------

export const getExpenseCategoriesAsyncThunk = createAsyncThunk(
    'expenseCategories/getExpenseCategories',
    async (): Promise<ExpenseCategory[]> => {
        const response: AxiosResponse<ExpenseCategory[]> = await getExpenseCategories();
        return response.data;
    }
)

export const postExpenseCategoryAsyncThunk = createAsyncThunk(
    'expenseCategories/postExpenseCategory',
    async (expenseCategory: ExpenseCategory): Promise<boolean> => {
        await postExpenseCategory(expenseCategory);
        return true;
    }
)

export const putExpenseCategoryAsyncThunk = createAsyncThunk(
    'expenseCategories/putExpenseCategory',
    async (expenseCategory: ExpenseCategory): Promise<boolean> => {
        await putExpenseCategory(expenseCategory);
        return true;
    }
)

export const deleteExpenseCategoryAsyncThunk = createAsyncThunk(
    'expenseCategories/deleteExpenseCategory',
    async (id: string): Promise<boolean> => {
        await deleteExpenseCategory(id);
        return true;
    }
)


export const deletingExpenseCategoryAsyncThunk = createAsyncThunk(
    'expenseCategories/deletingExpenseCategory',
    async (id: string): Promise<ExpenseCategory> => {
        const response: AxiosResponse<ExpenseCategory> = await getExpenseCategory(id);
        return response.data;
    }
)

export const editingExpenseCategoryAsyncThunk = createAsyncThunk(
    'expenseCategories/editingExpenseCategory',
    async (id: string): Promise<ExpenseCategory> => {
        const response: AxiosResponse<ExpenseCategory> = await getExpenseCategory(id);
        return response.data;
    }
)
//#endregion

const expenseCategoriesSlice = createSlice({
    name: 'expenseCategories',
    initialState: InitialExpenseCategoriesState,
    reducers: {
        addingExpenseCategory(state) {
            const expenseCategory: ExpenseCategory  = {
                id: zeroGuid,
                title: undefined,
                color: undefined
            };

            state.isAdding = true;
            state.current = expenseCategory;
        },
        clearExpenseCategory(state) {
            state.isAdding = false;
            state.isEditing = false;
            state.isDeleting = false;
            state.current = null;
            state.postedResult = null;
        },
        clearErrorExpenseCategories(state) {
            state.error = null;
        }
    },
    extraReducers: builder => {
        builder

        //#region ---------------- getExpenseCategories --------------------
        .addCase(getExpenseCategoriesAsyncThunk.pending, (state) => {
            state.expenseCategories = [];
            state.loading = true;
        })
        .addCase(getExpenseCategoriesAsyncThunk.fulfilled, (state, action: PayloadAction<ExpenseCategory[]>) => {
            state.expenseCategories = action.payload;
            state.loading = false;
            state.needToUpdate = false;
        })
        .addCase(getExpenseCategoriesAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить список категорий расходов', error: action.error.message };
            state.loading = false;
        })
        //#endregion

        //#region ---------------- postExpenseCategory --------------------
        .addCase(postExpenseCategoryAsyncThunk.pending, (state) => {
            state.posting = true;
        })
        .addCase(postExpenseCategoryAsyncThunk.fulfilled, (state) => {
            state.current = null;
            state.isAdding = false;
            state.needToUpdate = true;
            state.posting = false;
        })
        .addCase(postExpenseCategoryAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
            state.posting = false;
        })
        //#endregion

        //#region ---------------- putExpenseCategory --------------------
        .addCase(putExpenseCategoryAsyncThunk.pending, (state) => {
            state.posting = true;
        })
        .addCase(putExpenseCategoryAsyncThunk.fulfilled, (state) => {
            state.isEditing = false;
            state.needToUpdate = true;
            state.posting = false;
            state.current = null;
        })
        .addCase(putExpenseCategoryAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось сохранить изменения', error: action.error.message };
            state.posting = false;
        })
        //#endregion
    
        //#region ---------------- deleteExpenseCategory --------------------
        .addCase(deleteExpenseCategoryAsyncThunk.fulfilled, (state) => {
            state.isDeleting = false;
            state.needToUpdate = true;
            state.current = null;
        })
        .addCase(deleteExpenseCategoryAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось выполнить удаление', error: action.error.message };
        })
        //#endregion

        //#region ---------------- deletingExpenseCategory --------------------
        .addCase(deletingExpenseCategoryAsyncThunk.fulfilled, (state, action: PayloadAction<ExpenseCategory>) => {
            state.current = action.payload;
            state.isDeleting = true;
        })
        .addCase(deletingExpenseCategoryAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить информацию о категории', error: action.error.message };
        })
        //#endregion

        //#region ---------------- editingExpenseCategory --------------------
        .addCase(editingExpenseCategoryAsyncThunk.fulfilled, (state, action: PayloadAction<ExpenseCategory>) => {
            state.isEditing = true;
            state.current = action.payload;
        })
        .addCase(editingExpenseCategoryAsyncThunk.rejected, (state, action) => {
            state.error = { message: 'Не удалось получить информацию о категории', error: action.error.message };
        })
        //#endregion
    }
})

export const { addingExpenseCategory, clearExpenseCategory, clearErrorExpenseCategories } = expenseCategoriesSlice.actions
export default expenseCategoriesSlice.reducer