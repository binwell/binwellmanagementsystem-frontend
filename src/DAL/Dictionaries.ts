import baseAPI from './Config';
import { SERVER_URL } from './Constants';
import { AxiosResponse } from 'axios';

// Interfaces
export interface ExpenseCategory{
    id: string;
	title?: string;
	color?: string;
}

export interface EventLogCategory{
    id: string;
	title?: string;
	color?: string;
	limit: number;
}

// Fetch methods
// ExpenseCategories
export const getExpenseCategories = async (): Promise<AxiosResponse<ExpenseCategory[]>> => {
	return baseAPI.get(`${SERVER_URL}Dictionaries/ExpenseCategories/`);
}

export const getExpenseCategory = async (id: string): Promise<AxiosResponse<ExpenseCategory>> => {	
	return baseAPI.get(`${SERVER_URL}Dictionaries/ExpenseCategories/${id}`);
}

export const postExpenseCategory = async (expenseCategory: ExpenseCategory): Promise<AxiosResponse> => {
	return baseAPI.post(`${SERVER_URL}Dictionaries/ExpenseCategories/`, expenseCategory);	
}

export const putExpenseCategory = async (expenseCategory: ExpenseCategory): Promise<AxiosResponse> => {
	return baseAPI.put(`${SERVER_URL}Dictionaries/ExpenseCategories/${expenseCategory.id}`, expenseCategory);
}

export const deleteExpenseCategory = async (id: string): Promise<AxiosResponse> => {
	return baseAPI.delete(`${SERVER_URL}Dictionaries/ExpenseCategories/${id}`);
}

// EventCategories
export const getEventLogCategories = async (): Promise<AxiosResponse<EventLogCategory[]>> => {
	return baseAPI.get(`${SERVER_URL}Dictionaries/EventCategories/`);
}

export const getEventLogCategory = async (id: string): Promise<AxiosResponse<EventLogCategory>> => {
	return baseAPI.get(`${SERVER_URL}Dictionaries/EventCategories/${id}`);
}

export const postEventLogCategory = async (eventLogCategory: EventLogCategory): Promise<AxiosResponse> => {
	return baseAPI.post(`${SERVER_URL}Dictionaries/EventCategories/`, eventLogCategory);
}

export const putEventLogCategory = async (eventLogCategory: EventLogCategory): Promise<AxiosResponse> => {
	return baseAPI.put(`${SERVER_URL}Dictionaries/EventCategories/${eventLogCategory.id}`, eventLogCategory);
}

export const deleteEventLogCategory = async (id: string): Promise<AxiosResponse> => {
	return baseAPI.delete(`${SERVER_URL}Dictionaries/EventCategories/${id}`);
}
