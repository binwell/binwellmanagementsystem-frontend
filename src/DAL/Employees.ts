import baseAPI from './Config';
import { SERVER_URL } from './Constants';
import { stringify } from 'querystring';
import { AxiosResponse } from 'axios';

// Interfaces
export interface Employee{
    id: string;
	fullName: string;
	birthDate: Date;
	email: string;
	phone: string;
	employedDate: Date;
	leaveDate: Date | null;
	roles: string[];
	employmentType: string
}

export interface Role{
	id: string,
	title: string
}

export interface EmployeeEdit{
    id: string;
	fullName?: string;
	email?: string;
	phone?: string;
	birthDate: Date;
	employedDate: Date;
	leaveDate: Date | null;
	managerId: string;
	roles: Role [];
	employmentTypeId: string
}

export interface EmployeeFromServer{
    id: string;
	fullName: string;
	birthDate: string;
	email: string;
	phone: string;
	employedDate: string;
	leaveDate: string | null;
	roles: string[];
	manager: string | undefined;
	employmentType: string
}

export interface EmployeeEditFromServer{
    id: string;
	fullName: string;
	birthDate: string;
	email: string;
	phone: string;
	employedDate: string;
	leaveDate: string | null;
	managerId: string;
	roles: Role[];
	employmentTypeId: string
}

// Map methods
export const mapEmployeeFromServer = (employee: EmployeeFromServer): Employee => ({
	...employee, 
	birthDate: new Date(employee.birthDate),
	employedDate: new Date(employee.employedDate), 
	leaveDate: employee.leaveDate ? new Date(employee.leaveDate) : null
});

export const mapEmployeeEditFromServer = (employee: EmployeeEditFromServer): EmployeeEdit => ({
	...employee,
	birthDate: new Date(employee.birthDate),
	employedDate: new Date(employee.employedDate), 
	leaveDate: employee.leaveDate ? new Date(employee.leaveDate) : null,
	//managerId: Number.parseInt(employee.managerId)
	managerId: employee.managerId
});

// Const lists
export const EmploymentTypes = [
	{
		employmentTypeId: 'f015beaa-5fac-429e-9a3d-cc79f968b966',
		title: 'Полная занятость'
	},
	{
		employmentTypeId: '6e2bc367-f5ad-4679-af2e-5c47f2c7f57a',
		title: 'Частичная занятость'
	},
	{
		employmentTypeId: '3353ecd6-e6f7-4490-a7a4-a8b7e7e6f0da',
		title: 'Контрактор'
	}
]

// Fetch methods
export const getEmployees = async (includeFired: boolean | null): Promise<AxiosResponse<EmployeeFromServer[]>> => {
	const query={
		includeFired: includeFired
	}

	return baseAPI.get(`${SERVER_URL}Employees?${stringify(query)}`);
}

export const getEmployee = async (id: string): Promise<AxiosResponse<EmployeeEditFromServer>> => {	
	return baseAPI.get(`${SERVER_URL}Employees/${id}`);
}

export const postEmployee = async (employee: EmployeeEdit): Promise<AxiosResponse> => {
	return baseAPI.post(`${SERVER_URL}Employees/`, employee);
}

export const putEmployee = async (employee: EmployeeEdit): Promise<AxiosResponse> => {
	return baseAPI.put(`${SERVER_URL}Employees/${employee.id}`, employee);
}

export const deleteEmployee = async (id: string): Promise<AxiosResponse> => {
	return baseAPI.delete(`${SERVER_URL}Employees/${id}`);
}
