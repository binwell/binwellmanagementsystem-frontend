import baseAPI from './Config';
import { SERVER_URL } from './Constants';
import { getEmail } from '../shared/LocalStorageUtils';
import { AxiosResponse } from 'axios';

interface SendGridMsg{
    from: string,
    subject: string,
    text: string
}

export const sendFeedback = async (message: string): Promise<AxiosResponse> => {    
    
    const sendGridMsg: SendGridMsg = {
        from: getEmail(),
        subject: "BMS feedback",
        text: message
    }

    return baseAPI.post(`${SERVER_URL}SendGrid/`, sendGridMsg);
}