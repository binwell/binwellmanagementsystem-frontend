import CryptoJS from 'crypto-js';
import Axios, { AxiosResponse } from 'axios';
import { getEmail, getPassword, removeUserInfo } from '../shared/LocalStorageUtils';

type responseTypes = "arraybuffer" | "blob" | "document" | "json" | "text" | "stream" | undefined;

export default {

    setHeader(responseType?: responseTypes) {
        Axios.defaults.headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(getEmail() + ":" + getPassword()))
        };

        Axios.defaults.auth = { username: getEmail(), password: getPassword() };

        if(responseType)
            Axios.defaults.responseType = responseType;
    },

    // checkUnauthorizedUser(messageErr?: string) {
    //     if(messageErr && messageErr.includes('401'))
    //         removeUserInfo();
    // },

    get(url: string, responseType?: responseTypes): Promise<AxiosResponse> {

        Axios.defaults.withCredentials = true;

        if(responseType)
            Axios.defaults.responseType = responseType;

        return new Promise((resolve, reject) => {
            Axios.get(url)
                .then(response => {
                    resolve(response)
                })
                .catch(err => {
                    if(err.response.status === 401)
                        removeUserInfo();

                    reject(err);
                })
        })
    },

    post(url: string, data?: any): Promise<AxiosResponse> {

        Axios.defaults.withCredentials = true;

        return new Promise((resolve, reject) => {
            Axios.post(url, data)
                .then(response => {
                    resolve(response);
                })
                .catch(err => {
                    if(err.response.status === 401)
                        removeUserInfo();

                    reject(err);
                })
        })
    },

    put(url: string, data?: any): Promise<AxiosResponse> {

        console.log(JSON.stringify(data));        

        Axios.defaults.withCredentials = true;

        return new Promise((resolve, reject) => {
            Axios.put(url, data)
                .then(response => {
                    resolve(response);
                })
                .catch(err => {
                    if(err.response.status === 401)
                        removeUserInfo();

                    reject(err);
                })
        })
    },

    delete(url: string): Promise<AxiosResponse> {
        
        Axios.defaults.withCredentials = true;

        return new Promise((resolve, reject) => {
            Axios.delete(url)
                .then(response => {
                    resolve(response);
                })
                .catch(err => {
                    if(err.response.status === 401)
                        removeUserInfo();

                    reject(err);
                })
        })
    }
 }