import data from '../data.json';

export interface IProductionCalendar{
	year: number,
	months: number[][]
}

interface IProductionCalendarFromServer{
	"Год/Месяц": string,
    "Январь": string,
    "Февраль": string,
    "Март": string,
    "Апрель": string,
    "Май": string,
    "Июнь": string,
    "Июль": string,
    "Август": string,
    "Сентябрь": string,
    "Октябрь": string,
    "Ноябрь": string,
    "Декабрь": string,
    "Всего рабочих дней": string,
    "Всего праздничных и выходных дней": string,
    "Количество рабочих часов при 40-часовой рабочей неделе": string,
    "Количество рабочих часов при 36-часовой рабочей неделе": string,
    "Количество рабочих часов при 24-часовой рабочей неделе": string
}

export const getProductionCalendar = async(): Promise<any> => {
	return new Promise((resolve, reject)=>{
		resolve(data)
	});
	// return axios(SERVER_URL + 'govrucalendar', generateFetchConfigAxios('GET'));
}

// Returns array of nums unworking days for a month
const getProductionMonth = (monthString: string): number[] => {
	return monthString.split(',')
	// filter shortened days
	.filter(m=>!m.includes("*"))
	.map(item=>{return Number.parseInt(item)});	 
}

export const mapProductionCalendarFromServer = (productionCalendar: IProductionCalendarFromServer): IProductionCalendar => ({
	year: Number.parseInt(productionCalendar["Год/Месяц"]),
	months: [
		getProductionMonth(productionCalendar.Январь), 
		getProductionMonth(productionCalendar.Февраль),
		getProductionMonth(productionCalendar.Март),
		getProductionMonth(productionCalendar.Апрель),
		getProductionMonth(productionCalendar.Май),
		getProductionMonth(productionCalendar.Июнь),
		getProductionMonth(productionCalendar.Июль),
		getProductionMonth(productionCalendar.Август),
		getProductionMonth(productionCalendar.Сентябрь),
		getProductionMonth(productionCalendar.Октябрь),
		getProductionMonth(productionCalendar.Ноябрь),
		getProductionMonth(productionCalendar.Декабрь)
	]
});