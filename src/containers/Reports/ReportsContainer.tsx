import React from 'react';
import ContentContainer from '../ContentContainer/ContentContainer';
import SelectReportDialogComponent from '../../components/Reports/SelectReportDialogComponent';

const ReportsContainer = () => {
    return (
        <ContentContainer title="Отчеты" showContent={true}>            
            <SelectReportDialogComponent/>
        </ContentContainer>           
    )
}

export default ReportsContainer;