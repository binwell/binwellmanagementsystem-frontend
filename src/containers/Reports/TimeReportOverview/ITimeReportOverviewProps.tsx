import { Employee } from "../../../DAL/Employees"
import { TimeTracking } from "../../../DAL/TimeTracking"
import { ISearchProps, ActionAsyncThunk } from "../../../shared/Common"
import { getEmployeesAsyncThunk } from "../../../store/slice/employeesSlice"
import { AppState } from "../../../store/slice"
import { getTimeTrackingsAsyncThunk, GetTimeTrackingsInfoType } from "../../../store/slice/timeTrackingsSlice"

export interface ITimeReportOverviewProps{
    employees: Employee[] | null;
    getEmployees: () => ActionAsyncThunk<Employee[], boolean | undefined>;
    employeesLoading: boolean;

    timeTrackings: TimeTracking[] | null;
    getTimeTrackings: (timeTrackingsInfoArg: GetTimeTrackingsInfoType) => ActionAsyncThunk<TimeTracking[], GetTimeTrackingsInfoType>;
    timeTrackingsLoading: boolean;

    searchProps: ISearchProps;
}

export const mapStateToProps = (store: AppState) => {
    return{       
        employees: store.employees.employees,
        employeesLoading: store.employees.loading,

        timeTrackings: store.timeTrackings.timeTrackings,
        timeTrackingsLoading: store.timeTrackings.loading
    }
}

export const mapDispatchToProps = {
    getEmployees: () => getEmployeesAsyncThunk(),
    getTimeTrackings: (timeTrackingsInfoArg: GetTimeTrackingsInfoType) => getTimeTrackingsAsyncThunk(timeTrackingsInfoArg)
}