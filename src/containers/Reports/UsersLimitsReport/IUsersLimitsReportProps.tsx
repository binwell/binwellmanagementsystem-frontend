import { Employee } from "../../../DAL/Employees"
import { EventLog } from "../../../DAL/Calendar"
import { EventLogCategory } from "../../../DAL/Dictionaries"
import { IProductionCalendar } from "../../../DAL/ProductionCalendar"

import { getEmployeesAsyncThunk } from "../../../store/slice/employeesSlice"
import { getEventLogsAsyncThunk, EventLogsInfoType } from "../../../store/slice/calendarSlice"
import { getEventLogCategoriesAsyncThunk } from "../../../store/slice/eventLogCategoriesSlice"
import { getProductionCalendarAsyncThunk } from "../../../store/slice/productionCalendarSlice"
import { AppState } from "../../../store/slice"
import { ActionAsyncThunk } from "../../../shared/Common"

export interface IUsersLimitsReportProps {
    employees: Employee[] | null;
    employeesLoading: boolean;
    getEmployees: () => ActionAsyncThunk<Employee[], boolean | undefined>;
    
    eventLogs: EventLog[] | null;
    eventLogsLoading: boolean;
    getEventLogs: (eventLogsInfoArg: EventLogsInfoType) => ActionAsyncThunk<EventLog[], EventLogsInfoType>;

    getEventLogCategories: () => ActionAsyncThunk<EventLogCategory[], void>;
    eventLogCategories: EventLogCategory[] | null;
    eventLogCategoriesLoading: boolean; 

    productionCalendar: IProductionCalendar[];
    getProductionCalendar: () => ActionAsyncThunk<IProductionCalendar[], void> ;
    productionCalendarLoading: boolean;
}

export const mapStateToProps = (store: AppState) => {
    return {
        employees: store.employees.employees,
        employeesLoading: store.employees.loading,

        eventLogs: store.eventLogs.eventLogs,
        eventLogsLoading: store.eventLogs.loading,

        eventLogCategories: store.eventLogCategories.eventLogCategories,
        eventLogCategoriesLoading: store.eventLogCategories.loading,

        productionCalendar: store.productionCalendar.productionCalendar,
        productionCalendarLoading: store.productionCalendar.loading
    }
}

export const mapDispacthToProps = {
    getEmployees: () => getEmployeesAsyncThunk(),

    getEventLogs: (eventLogsInfoArg: EventLogsInfoType) => getEventLogsAsyncThunk(eventLogsInfoArg),
    getEventLogCategories: () => getEventLogCategoriesAsyncThunk(),
    getProductionCalendar: () => getProductionCalendarAsyncThunk()
}