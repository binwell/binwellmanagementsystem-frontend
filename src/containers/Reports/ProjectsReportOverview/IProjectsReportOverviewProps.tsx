import { Project } from "../../../DAL/Projects"
import { TimeTracking } from "../../../DAL/TimeTracking"
import { ISearchProps, ActionAsyncThunk } from "../../../shared/Common"
import { getProjectsAsyncThunk } from "../../../store/slice/projectsSlice"
import { AppState } from "../../../store/slice"
import { getTimeTrackingsAsyncThunk, GetTimeTrackingsInfoType } from "../../../store/slice/timeTrackingsSlice"

export interface IProjectsReportOverviewProps{
    projects: Project[] | null;
    getProjects: () => ActionAsyncThunk<Project[], string | null | undefined>;
    projectsLoading: boolean;

    timeTrackings: TimeTracking[] | null;
    getTimeTrackings:  (timeTrackingsInfoArg: GetTimeTrackingsInfoType) => ActionAsyncThunk<TimeTracking[], GetTimeTrackingsInfoType>;
    timeTrackingsLoading: boolean;

    searchProps: ISearchProps;
}


export const mapStateToProps = (store: AppState) => {
    return{       
        projects: store.projects.projects,
        projectsLoading: store.projects.loading,

        timeTrackings: store.timeTrackings.timeTrackings,
        timeTrackingsLoading: store.timeTrackings.loading
    }
}

export const mapDispatchToProps = {
    getProjects: () => getProjectsAsyncThunk(),

    getTimeTrackings: (timeTrackingsInfoArg: GetTimeTrackingsInfoType) => getTimeTrackingsAsyncThunk(timeTrackingsInfoArg),
}