import React, { FC, useEffect, useState } from 'react';

import { ComboBox, IComboBoxOption, IComboBox, Label } from '@fluentui/react';

import EventLogsOverviewReportComponent from '../../../components/Reports/EventLogsOverviewReportComponent';
import EventLogsDetailsReportComponent from '../../../components/Reports/EventLogsDetailsReportComponent';
import SelectPeriodComponent from '../../../components/Reports/SelectPeriodComponent';
import ContentContainer from '../../ContentContainer/ContentContainer';
import { APPROVED } from '../../../shared/Constants';
import DetailToggleComponent from '../../../components/Reports/DetailToggleComponent';

import { connect } from 'react-redux';
import { IEventLogsReportOverviewProps, mapStateToProps, mapDispatchToProps } from './IEventLogsReportOverviewProps';

const EventLogsReportOverviewContainer: FC<IEventLogsReportOverviewProps> = (props: IEventLogsReportOverviewProps) => {

    const [EmployeesOptions, setEmployeesOptions] = useState<IComboBoxOption[]>();
    const [currentEmployeeId, setCurrentEmployeeId] = useState<string>('');

    useEffect(()=>{
        props.getEmployees();
    }, [props.getEmployees]);

    useEffect(()=>{
        if(props.searchProps.fromDate && props.searchProps.toDate){
            props.getEventLogs({ employeeId: null, status: APPROVED, fromDate: props.searchProps.fromDate, toDate: props.searchProps.toDate});
            props.getEventLogCategories();
            props.getProductionCalendar();
        }            
    }, [props.getEventLogCategories, props.getEventLogs, props.getProductionCalendar, props.searchProps.fromDate, props.searchProps.toDate]);

    // Set employees list options
    useEffect(()=>{
        let options: IComboBoxOption[] = [{
            key: '',
            text: "Все сотрудники"
        } as IComboBoxOption];

        if(props.employees)
            options = options.concat(props.employees.map(item=>{
                return{
                    key: item.id,
                    text: item.fullName
                } as IComboBoxOption
            }));
        setEmployeesOptions(options);                
    }, [props.employees]);

    const _onChangeCurrentEmployee = (event: React.FormEvent<IComboBox>, option?: IComboBoxOption, index?: number, value?: string): void => {
        if(option){
            //setCurrentEmployeeId(Number.parseInt(option.key.toString()));
            setCurrentEmployeeId(option.key.toString());
        }
    }

    return (
        <ContentContainer title="Отчет по нерабочему времени" showContent={props.employees!==null && props.productionCalendar!==null && props.eventLogCategories!==null && props.eventLogs!==null}>
            <SelectPeriodComponent fromDate={props.searchProps.fromDate} toDate={props.searchProps.toDate}/>

            <DetailToggleComponent isDetail={props.searchProps.isDetail}/>

            {props.searchProps.isDetail && !(props.eventLogs?.length===0 || props.eventLogCategories?.length===0 || props.employees?.length===0) &&
            <ComboBox 
                label="Сотрудник" 
                options={EmployeesOptions} 
                selectedKey={currentEmployeeId} 
                onChange={_onChangeCurrentEmployee}          
                className="combobox-employee"        
            />}         

            {(props.eventLogs?.length===0 || props.eventLogCategories?.length===0) && !(props.eventLogsLoaging || props.eventLogCategoriesLoading) &&
            <Label className="text-center">
                Нет данных для отображения
            </Label>}

            {props.searchProps.isDetail && !(props.eventLogs?.length===0 || props.eventLogCategories?.length===0) &&
            <EventLogsDetailsReportComponent
                eventLogs={currentEmployeeId ? props.eventLogs?.filter(el => el.employeeId === currentEmployeeId) || [] : props.eventLogs || []}
                isLoading={props.employeesLoading || props.eventLogsLoaging || props.eventLogCategoriesLoading || props.productionCalendarLoading}
                eventLogCategories={props.eventLogCategories || []}
                productionCalendar={props.productionCalendar}
            />
            }

            {!props.searchProps.isDetail && !(props.eventLogs?.length===0 || props.eventLogCategories?.length===0) &&
            <EventLogsOverviewReportComponent
                eventLogs={props.eventLogs || []}
                eventLogsCategories={props.eventLogCategories || []}
                isLoading={props.eventLogsLoaging || props.eventLogCategoriesLoading || props.productionCalendarLoading}
                productionCalendar={props.productionCalendar || []}
            />}
            
        </ContentContainer>           
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(EventLogsReportOverviewContainer);