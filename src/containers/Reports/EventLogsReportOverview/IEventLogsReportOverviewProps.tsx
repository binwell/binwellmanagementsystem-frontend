import { Employee } from "../../../DAL/Employees"
import { EventLog } from "../../../DAL/Calendar"
import { EventLogCategory } from "../../../DAL/Dictionaries"
import { ISearchProps, ActionAsyncThunk } from "../../../shared/Common"
import { IProductionCalendar } from "../../../DAL/ProductionCalendar"

import { getEmployeesAsyncThunk } from "../../../store/slice/employeesSlice"
import { AppState } from "../../../store/slice"
import { getEventLogsAsyncThunk, EventLogsInfoType } from "../../../store/slice/calendarSlice"
import { getEventLogCategoriesAsyncThunk } from "../../../store/slice/eventLogCategoriesSlice"
import { getProductionCalendarAsyncThunk } from "../../../store/slice/productionCalendarSlice"

export interface IEventLogsReportOverviewProps{
    employees: Employee[] | null;
    getEmployees: () => ActionAsyncThunk<Employee[], boolean | undefined>;
    employeesLoading: boolean;

    eventLogs: EventLog[] | null;
    eventLogsLoaging: boolean;
    getEventLogs: (eventLogsInfoArg: EventLogsInfoType) => ActionAsyncThunk<EventLog[], EventLogsInfoType>;
    
    eventLogCategories: EventLogCategory[] | null;
    eventLogCategoriesLoading: boolean;
    getEventLogCategories: () => ActionAsyncThunk<EventLogCategory[], void>;

    searchProps: ISearchProps;

    productionCalendar: IProductionCalendar[];
    getProductionCalendar: () => ActionAsyncThunk<IProductionCalendar[], void>;
    productionCalendarLoading: boolean;
}

export const mapStateToProps = (store: AppState) => {
    return{       
        employees: store.employees.employees,
        employeesLoading: store.employees.loading,
        eventLogs: store.eventLogs.eventLogs,
        eventLogsLoaging: store.eventLogs.loading,
        eventLogCategories: store.eventLogCategories.eventLogCategories,
        eventLogCategoriesLoading: store.eventLogCategories.loading,

        productionCalendar: store.productionCalendar.productionCalendar,
        productionCalendarLoading: store.productionCalendar.loading
    }
}

export const mapDispatchToProps = {
    getEmployees: () => getEmployeesAsyncThunk(),

    getEventLogs: (eventLogsInfoArg: EventLogsInfoType) => getEventLogsAsyncThunk(eventLogsInfoArg),
    getEventLogCategories: () => getEventLogCategoriesAsyncThunk(),
    getProductionCalendar: () => getProductionCalendarAsyncThunk()
}