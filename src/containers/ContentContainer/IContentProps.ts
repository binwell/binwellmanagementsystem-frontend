import { AppState } from "../../store/slice";
import { ErrorObject } from "../../shared/Common";
import { clearErrorAssets } from "../../store/slice/assetsSlice";
import { clearErrorCalendar } from "../../store/slice/calendarSlice";
import { clearErrorEmployees } from "../../store/slice/employeesSlice";
import { clearErrorExpenses } from "../../store/slice/expensesSlice";
import { clearErrorFeedback } from "../../store/slice/feedbackSlice";
import { clearErrorProductionCalendar } from "../../store/slice/productionCalendarSlice";
import { clearErrorProjects } from "../../store/slice/projectsSlice";
import { clearErrorTimeTrackings } from "../../store/slice/timeTrackingsSlice";
import { clearErrorWorkTasks } from "../../store/slice/workTasksSlice";
import { clearErrorExpenseCategories } from "../../store/slice/expenseCategoriesSlice";
import { clearErrorEventLogCategories } from "../../store/slice/eventLogCategoriesSlice";

export interface IContentProps {    
    title?: string;
    showContent?: boolean;
    children: React.ReactNode;

    errAssets: ErrorObject | null;
    errCalendar: ErrorObject | null;
    errEmployees: ErrorObject | null;
    errEventLogCategories: ErrorObject | null;
    errExpenseCategories: ErrorObject | null;
    errExpenses: ErrorObject | null;
    errFeedback: ErrorObject | null;
    errProductionCalendar: ErrorObject | null;
    errProjects: ErrorObject | null;
    errTimeTrackings: ErrorObject | null;
    errWorkTasks: ErrorObject | null;

    clearErrAssets: () => void;
    clearErrCalendar: () => void;
    clearErrEmployees: () => void;
    clearErrExpenseCategories: () => void;
    clearErrorEventLogCategory: () => void;
    clearErrExpenses: () => void;
    clearErrFeedback: () => void;    
    clearErrProductionCalendar: () => void;
    clearErrProjects: () => void;
    clearErrTimeTrackings: () => void;
    clearErrWorkTasks: () => void;
}

export const mapStateToProps = (store: AppState) =>{
    return {
        errAssets: store.assets.error,
        errCalendar: store.eventLogs.error,
        errEmployees: store.employees.error,
        errEventLogCategories: store.eventLogCategories.error,
        errExpenseCategories: store.expenseCategories.error,
        errExpenses: store.expenses.error,
        errFeedback: store.feedback.error,
        errProductionCalendar: store.productionCalendar.error,
        errProjects: store.projects.error,
        errTimeTrackings: store.timeTrackings.error,
        errWorkTasks: store.workTasks.error
    }
}

export const mapDispatchToProps = {
    clearErrAssets: () => clearErrorAssets(),
    clearErrCalendar: () => clearErrorCalendar(),
    clearErrEmployees: () => clearErrorEmployees(),    
    clearErrExpenseCategories: () => clearErrorExpenseCategories(),
    clearErrorEventLogCategory: () => clearErrorEventLogCategories(),
    clearErrExpenses: () => clearErrorExpenses(),
    clearErrFeedback: () => clearErrorFeedback(),
    clearErrProductionCalendar: () => clearErrorProductionCalendar(),
    clearErrProjects: () => clearErrorProjects(),
    clearErrTimeTrackings: () => clearErrorTimeTrackings(),
    clearErrWorkTasks: () => clearErrorWorkTasks()
}