import React from 'react';

import ProductionCalendarComponent from '../../components/ProductionCalendar/ProductionCalendarComponent'

const ProductionCalendarContainer = () => {
    return(
        <ProductionCalendarComponent/>
    )
}

export default ProductionCalendarContainer;