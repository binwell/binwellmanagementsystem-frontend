import React, { FC, useEffect } from 'react';
import { Label } from '@fluentui/react';
import ExpenseConfirmationsListComponent from '../../../components/Confirmations/ExpenseConfirmationsListComponent';
import EventLogConfirmationsListComponent from '../../../components/Confirmations/EventLogConfirmationsListComponent';
import ContentContainer from '../../ContentContainer/ContentContainer';
import DeleteDialog from '../../../components/DeleteDialog';

import { connect } from 'react-redux';

import { CREATED, zeroGuid } from '../../../shared/Constants';
import { IConfirmationsProps, mapStateToProps, mapDispatchToProps} from './IConfirmationsProps';

const ConfirmationsContainer: FC<IConfirmationsProps> = (props: IConfirmationsProps) => {

    useEffect(()=>{

        if(props.needToUpdateExpenses)
            props.getExpenses({employeeId: null, status: CREATED, fromDate: null, toDate: null});
    
    }, [props.getExpenses, props.needToUpdateExpenses]);

    useEffect(()=>{

        if(props.needToUpdateEventLogs)
            props.getEventLogs({employeeId: null, status: CREATED, fromDate: null, toDate: null});      
    
    }, [props.getEventLogs, props.needToUpdateEventLogs]);    

    const expenseId = props.currentExpense ? props.currentExpense.id : zeroGuid;
    const eventLogId = props.currentEventLog ? props.currentEventLog.id : zeroGuid;

    return (
        <>
            {/* Expenses confirmations */}
            <ContentContainer 
                title="Подтверждения расходов" 
                showContent={true}
            >                
                <ExpenseConfirmationsListComponent 
                    data={props.expenses || []} 
                    patchExpense={props.patchExpense} 
                    isLoading ={props.expensesLoading}
                    deletingExpense={props.deletingExpense}
                    saveFile={props.saveFile}
                />

                {!props.expensesLoading && props.expenses?.length===0 && 
                <Label className="text-center">
                    Нет данных для отображения
                </Label>}

                {props.isDeletingExpense && props.currentExpense && 
                <DeleteDialog
                    hidden={!props.isDeletingExpense}
                    deleteMethod={()=>props.deleteExpense(expenseId)}
                    closeMethod={()=>props.clearExpense()}
                />}             
            </ContentContainer>  

            {/* EventLogs confirmations */}
            <ContentContainer 
                title="Подтверждения изменений в каледаре" 
                showContent={true}
            >                             
                <EventLogConfirmationsListComponent 
                    data={props.eventLogs || []} 
                    patchEventLog={props.patchEventLog} 
                    isLoading={props.eventLogsLoading}
                    deletingEventLog={props.deletingEventLog}
                />

                {!props.eventLogsLoading && props.eventLogs?.length===0 && 
                <Label className="text-center">
                    Нет данных для отображения
                </Label>}

                {props.isDeletingEventLog && props.currentEventLog && 
                <DeleteDialog
                    hidden={!props.isDeletingEventLog}
                    deleteMethod={()=>props.deleteEventLog(eventLogId)}
                    closeMethod={()=>props.clearEventLog()}
                />}            
            </ContentContainer>      
        </>  
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmationsContainer);