import { Expense, ExpenseEdit } from "../../../DAL/Expenses"
import { EventLog, EventLogEdit } from "../../../DAL/Calendar"

import { clearExpense,
         getExpensesAsyncThunk, 
         patchExpenseAsyncThunk,           
         deleteExpenseAsyncThunk, 
         deletingExpenseAsyncThunk, 
         saveDocumentAsyncThunk, 
         ExpensesInfoType,
         PatchExpenseInfoType,
         DocumentInfoType
} from "../../../store/slice/expensesSlice"

import { getEventLogsAsyncThunk, patchEventLogAsyncThunk, clearEventLog, deleteEventLogAsyncThunk, deletingEvenLogAsyncThunk, EventLogsInfoType, PatchEventLogInfoType } from "../../../store/slice/calendarSlice"
import { AppState } from "../../../store/slice";
import { ActionAsyncThunk } from "../../../shared/Common";

export interface IConfirmationsProps {
    expenses: Expense[] | null;
    expensesLoading: boolean;
    isDeletingExpense: boolean;
    currentExpense: ExpenseEdit | null;
    expensesPosting: boolean;
    getExpenses: (expensesArg: ExpensesInfoType) => ActionAsyncThunk<Expense[], ExpensesInfoType>;
    patchExpense: (patchExpenseArg: PatchExpenseInfoType) => ActionAsyncThunk<boolean, PatchExpenseInfoType>;
    clearExpense: () => void;
    deletingExpense: (id: string) => ActionAsyncThunk<ExpenseEdit, string>;
    deleteExpense: (id: string) => ActionAsyncThunk<boolean, string>;
    saveFile: (documentInfoArg: DocumentInfoType) => ActionAsyncThunk<boolean, DocumentInfoType>;

    eventLogs: EventLog[] | null;
    eventLogsLoading: boolean;
    isDeletingEventLog: boolean;
    currentEventLog: EventLogEdit | null;
    eventLogsPosting: boolean;
    getEventLogs: (eventLogsInfoArg: EventLogsInfoType) => ActionAsyncThunk<EventLog[], EventLogsInfoType>;
    patchEventLog: (patchEventLogInfoArg: PatchEventLogInfoType) => ActionAsyncThunk<boolean, PatchEventLogInfoType>; 
    clearEventLog: () => void;
    deletingEventLog: (id: string) => ActionAsyncThunk<EventLogEdit, string>;
    deleteEventLog: (id: string) => ActionAsyncThunk<boolean, string>;

    needToUpdateEventLogs: boolean;
    needToUpdateExpenses: boolean;
}

export const mapStateToProps = (store: AppState) => {
    return{
        expenses: store.expenses.expenses,
        expensesLoading: store.expenses.loading,
        isDeletingExpense: store.expenses.isDeleting,
        currentExpense: store.expenses.current,
        expensesPosting: store.expenses.posting,

        eventLogs: store.eventLogs.eventLogs,
        eventLogsLoading: store.eventLogs.loading,
        isDeletingEventLog: store.eventLogs.isDeleting,
        currentEventLog: store.eventLogs.current,
        eventLogsPosting: store.eventLogs.posting,

        needToUpdateEventLogs: store.eventLogs.needToUpdate,
        needToUpdateExpenses: store.expenses.needToUpdate
    }
}

export const mapDispatchToProps = {
    getExpenses: (expensesArg: ExpensesInfoType) => getExpensesAsyncThunk(expensesArg),
    patchExpense: (patchExpenseArg: PatchExpenseInfoType) => patchExpenseAsyncThunk(patchExpenseArg),
    clearExpense: () => clearExpense(),
    deleteExpense: (id: string) => deleteExpenseAsyncThunk(id),
    deletingExpense: (id: string) => deletingExpenseAsyncThunk(id),
    saveFile: (documentInfoArg: DocumentInfoType) => saveDocumentAsyncThunk(documentInfoArg),

    getEventLogs: (eventLogsInfoArg: EventLogsInfoType) => getEventLogsAsyncThunk(eventLogsInfoArg),
    patchEventLog: (patchEventLogInfoArg: PatchEventLogInfoType) => patchEventLogAsyncThunk(patchEventLogInfoArg),
    clearEventLog: () => clearEventLog(),
    deleteEventLog: (id: string) => deleteEventLogAsyncThunk(id),
    deletingEventLog: (id: string) => deletingEvenLogAsyncThunk(id)
}