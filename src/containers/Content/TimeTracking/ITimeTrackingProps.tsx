import { Project } from "../../../DAL/Projects"
import { TimeTracking, TimeTrackingEdit, WorkTask } from "../../../DAL/TimeTracking"
import { ISearchProps, ActionAsyncThunk } from "../../../shared/Common"
import { getProjectsAsyncThunk } from "../../../store/slice/projectsSlice"
import { AppState } from "../../../store/slice"

import { getTimeTrackingsAsyncThunk, 
         addingTimeTracking, 
         clearTimeTracking,
         editingTimeTrackingAsyncThunk, 
         postTimeTrackingAsyncThunk, 
         putTimeTrackingAsyncThunk,           
         deleteTimeTrackingAsyncThunk, 
         GetTimeTrackingsInfoType
} from "../../../store/slice/timeTrackingsSlice"

import { getWorkTasksAsyncThunk, addingWorkTask, postWorkTaskAsyncThunk, clearWorkTask } from "../../../store/slice/workTasksSlice"

export interface ITimeTrackingProps {
    userId: string;

    projects: Project[] | null;
    projectsLoading: boolean;
    getProjects: (employee: string) => ActionAsyncThunk<Project[], string | null | undefined>;

    timeTrackings: TimeTracking[] | null;
    timeTrackingsLoading: boolean;
    currentTimeTracking: TimeTrackingEdit | null;
    isTimeTrackingAdding: boolean;
    isTimeTrackingEditing: boolean;
    timeTrackingsPosting: boolean;
    addingTimeTracking: (workTaskId?: string | null, startDate?: Date | null) => void;
    clearTimeTracking: () => void;
    getTimeTrackings: (timeTrackingsInfoArg: GetTimeTrackingsInfoType) => ActionAsyncThunk<TimeTracking[], GetTimeTrackingsInfoType>;
    editingTimeTracking: (id: string) => ActionAsyncThunk<TimeTrackingEdit, string>;
    postTimeTracking: (timeTracking: TimeTrackingEdit) => ActionAsyncThunk<boolean, TimeTrackingEdit>;
    putTimeTracking: (timeTracking: TimeTrackingEdit) => any;
    deleteTimeTracking: (id: string) => ActionAsyncThunk<boolean, string>;   
    
    workTasks: WorkTask[] | null;
    workTasksLoading: boolean;
    currentWorkTask: WorkTask | null;
    isWorkTaskAdding: boolean;
    workTasksPosting: boolean;
    clearWorkTask: () => void;
    addingWorkTask: () => void;
    getWorkTasks: (employeeId: string) => ActionAsyncThunk<WorkTask[], string | null | undefined>;    
    postWorkTask: (workTask: WorkTask) => ActionAsyncThunk<boolean, WorkTask>;
    

    needToUpdateWorkTasks: boolean;
    needToUpdateTimeTrackings: boolean;

    searchProps: ISearchProps;
}

export const mapStateToProps = (store: AppState) =>{
    return{
        userId: store.account.userId,

        projects: store.projects.projects,
        projectsLoading: store.projects.loading,

        timeTrackings: store.timeTrackings.timeTrackings,
        timeTrackingsLoading: store.timeTrackings.loading,
        currentTimeTracking: store.timeTrackings.current,
        isTimeTrackingAdding: store.timeTrackings.isAdding,
        isTimeTrackingEditing: store.timeTrackings.isEditing,
        timeTrackingsPosting: store.timeTrackings.posting,

        workTasks: store.workTasks.workTasks,
        workTasksLoading: store.workTasks.loading,
        currentWorkTask: store.workTasks.current,
        isWorkTaskAdding: store.workTasks.isAdding,
        workTasksPosting: store.workTasks.posting,

        needToUpdateTimeTrackings: store.timeTrackings.needToUpdate,
        needToUpdateWorkTasks: store.workTasks.needToUpdate
    }
}

export const mapDispatchToProps = {
    getProjects: (employee: string)=> getProjectsAsyncThunk(employee),

    getTimeTrackings: (timeTrackingsInfoArg: GetTimeTrackingsInfoType) => getTimeTrackingsAsyncThunk(timeTrackingsInfoArg),

    addingTimeTracking: (workTaskId?: string | null, startDate?: Date | null) => addingTimeTracking(workTaskId, startDate),
    editingTimeTracking: (id: string) => editingTimeTrackingAsyncThunk(id),
    postTimeTracking: (timeTracking: TimeTrackingEdit) => postTimeTrackingAsyncThunk(timeTracking),
    putTimeTracking: (timeTracking: TimeTrackingEdit) => putTimeTrackingAsyncThunk(timeTracking),
    clearTimeTracking: () => clearTimeTracking(),
    deleteTimeTracking: (id: string) => deleteTimeTrackingAsyncThunk(id),

    getWorkTasks: (employeeId: string) => getWorkTasksAsyncThunk(employeeId),
    addingWorkTask: () => addingWorkTask(),
    postWorkTask: (workTask: WorkTask) => postWorkTaskAsyncThunk(workTask),
    clearWorkTask: () => clearWorkTask()
}