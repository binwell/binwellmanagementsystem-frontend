import { EmployeeEdit } from "../../../DAL/Employees"
import { EventLog } from "../../../DAL/Calendar"
import { EventLogCategory } from "../../../DAL/Dictionaries"
import { IProductionCalendar } from "../../../DAL/ProductionCalendar"

import { AppState } from "../../../store/slice"
import { getEventLogsAsyncThunk, EventLogsInfoType } from "../../../store/slice/calendarSlice"
import { getEventLogCategoriesAsyncThunk } from "../../../store/slice/eventLogCategoriesSlice"
import { getProductionCalendarAsyncThunk } from "../../../store/slice/productionCalendarSlice"
import { getEmployeeAsyncThunk } from "../../../store/slice/employeesSlice"
import { ActionAsyncThunk } from "../../../shared/Common"

export interface IUserLimitsProps {
    userId: string;
    employee: EmployeeEdit | null;
    getEmployee: (employeeId: string) => ActionAsyncThunk<EmployeeEdit, string>;
    
    eventLogs: EventLog[] | null;
    eventLogsLoading: boolean;
    getEventLogs: (eventLogsInfoArg: EventLogsInfoType) => ActionAsyncThunk<EventLog[], EventLogsInfoType>;

    getEventLogCategories: () => ActionAsyncThunk<EventLogCategory[], void>;
    eventLogCategories: EventLogCategory[] | null;
    eventLogCategoriesLoading: boolean; 

    productionCalendar: IProductionCalendar[];
    getProductionCalendar: () => ActionAsyncThunk<IProductionCalendar[], void>;
    productionCalendarLoading: boolean;
}

export const mapStateToProps = (store: AppState) => {
    return {
        userId: store.account.userId,
        employee: store.employees.current,

        eventLogs: store.eventLogs.eventLogs,
        eventLogsLoading: store.eventLogs.loading,

        eventLogCategories: store.eventLogCategories.eventLogCategories,
        eventLogCategoriesLoading: store.eventLogCategories.loading,

        productionCalendar: store.productionCalendar.productionCalendar,
        productionCalendarLoading: store.productionCalendar.loading
    }
}

export const mapDispacthToProps = {
    getEmployee: (employeeId: string) => getEmployeeAsyncThunk(employeeId),

    getEventLogs: (eventLogsInfoArg: EventLogsInfoType) => getEventLogsAsyncThunk(eventLogsInfoArg),
    getEventLogCategories: () => getEventLogCategoriesAsyncThunk(),
    getProductionCalendar: () => getProductionCalendarAsyncThunk()
}