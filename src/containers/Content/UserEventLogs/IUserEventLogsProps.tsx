import { RouteComponentProps } from "react-router-dom"
import { EventLog } from "../../../DAL/Calendar"
import { IProductionCalendar } from "../../../DAL/ProductionCalendar"
import { EventLogCategory } from "../../../DAL/Dictionaries"
import { ISearchProps, ActionAsyncThunk } from "../../../shared/Common"

import { getEventLogsAsyncThunk, EventLogsInfoType } from "../../../store/slice/calendarSlice"
import { getEventLogCategoriesAsyncThunk } from "../../../store/slice/eventLogCategoriesSlice"
import { getProductionCalendarAsyncThunk } from "../../../store/slice/productionCalendarSlice"
import { AppState } from "../../../store/slice"

export interface IUserEventLogsProps extends RouteComponentProps {
    userId: string;
    eventLogs: EventLog[];
    eventLogsLoading: boolean;
    getEventLogs: (eventLogsInfoArg: EventLogsInfoType) => ActionAsyncThunk<EventLog[], EventLogsInfoType>;
    searchProps: ISearchProps;
    eventLogCategories: EventLogCategory[];
    getEventLogCategories: () => ActionAsyncThunk<EventLogCategory[], void>;
    productionCalendar: IProductionCalendar[];
    getProductionCalendar: () => ActionAsyncThunk<IProductionCalendar[], void>;
    productionCalendarLoading: boolean;
}


export const mapStateToProps = (store: AppState) => {
    return {
        userId: store.account.userId,
        eventLogs: store.eventLogs.eventLogs,
        eventLogCategories: store.eventLogCategories.eventLogCategories,
        eventLogsLoading: store.eventLogs.loading,
        productionCalendar: store.productionCalendar.productionCalendar,
        productionCalendarLoading: store.productionCalendar.loading
    }
}

export const mapDispacthToProps = {
    getEventLogs: (eventLogsInfoArg: EventLogsInfoType) => getEventLogsAsyncThunk(eventLogsInfoArg),
    getEventLogCategories: () => getEventLogCategoriesAsyncThunk(),
    getProductionCalendar: () => getProductionCalendarAsyncThunk()
}