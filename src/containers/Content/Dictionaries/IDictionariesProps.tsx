import { EventLogCategory, ExpenseCategory } from "../../../DAL/Dictionaries"

import { AppState } from "../../../store/slice"
import { addingEventLogCategory,
         clearEventLogCategory, 
         getEventLogCategoriesAsyncThunk, 
         editingEventLogCategoryAsyncThunk, 
         postEventLogCategoryAsyncThunk, 
         putEventLogCategoryAsyncThunk,  
         deletingEventLogCategoryAsyncThunk, 
         deleteEventLogCategoryAsyncThunk 
} from "../../../store/slice/eventLogCategoriesSlice"

import { addingExpenseCategory,
         clearExpenseCategory,
         getExpenseCategoriesAsyncThunk, 
         editingExpenseCategoryAsyncThunk, 
         postExpenseCategoryAsyncThunk, 
         putExpenseCategoryAsyncThunk,           
         deletingExpenseCategoryAsyncThunk, 
         deleteExpenseCategoryAsyncThunk 
} from "../../../store/slice/expenseCategoriesSlice"
import { ActionAsyncThunk } from "../../../shared/Common"


export interface IDictionariesProps {    
    eventLogCategories: EventLogCategory[] | null;
    eventLogCategoriesLoading: boolean;
    currentEventLogCategory: EventLogCategory | null;
    isEventLogCategoryAdding: boolean;
    isEventLogCategoryEditing: boolean;
    isEventLogCategoryDeleting: boolean;
    eventLogCategoriesPosting: boolean;
    addingEventLogCategory: () => void;
    clearEventLogCategory: () => void;

    getEventLogCategories: () => ActionAsyncThunk<EventLogCategory[], void>;
    editingEventLogCategory: (id: string) => ActionAsyncThunk<EventLogCategory, string>; 
    postEventLogCategory: (eventLogCategory: EventLogCategory) => ActionAsyncThunk<boolean, EventLogCategory>;
    putEventLogCategory: (eventLogCategory: EventLogCategory) => ActionAsyncThunk<boolean, EventLogCategory>;
    deletingEventLogCategory: (id: string) => ActionAsyncThunk<EventLogCategory, string>;
    deleteEventLogCategory: (id: string) => ActionAsyncThunk<boolean, string>;    

    expenseCategories: ExpenseCategory[] | null;
    expenseCategoriesLoading: boolean;
    currentExpenseCategory: ExpenseCategory | null;
    isExpenseCategoryAdding: boolean;
    isExpenseCategoryEditing: boolean;
    isExpenseCategoryDeleting: boolean;
    expenseCategoriesPosting: boolean;
    addingExpenseCategory: () => void;
    clearExpenseCategory: () => void;

    getExpenseCategories: () => ActionAsyncThunk<ExpenseCategory[], void>;
    editingExpenseCategory: (id: string) => ActionAsyncThunk<ExpenseCategory, string>; 
    postExpenseCategory: (expenseCategory: ExpenseCategory) => ActionAsyncThunk<boolean, ExpenseCategory>;
    putExpenseCategory: (expenseCategory: ExpenseCategory) => ActionAsyncThunk<boolean, ExpenseCategory>;
    deletingExpenseCategory: (id: string) => ActionAsyncThunk<ExpenseCategory, string>;
    deleteExpenseCategory: (id: string) => ActionAsyncThunk<boolean, string>;    

    needToUpdateEventLogCategories: boolean;
    needToUpdateExpenseCategories: boolean;
}


export const mapStateToProps = (store: AppState) => {
    return {
        eventLogCategories: store.eventLogCategories.eventLogCategories,
        eventLogCategoriesLoading: store.eventLogCategories.loading,
        currentEventLogCategory: store.eventLogCategories.current,
        isEventLogCategoryAdding: store.eventLogCategories.isAdding,        
        isEventLogCategoryEditing: store.eventLogCategories.isEditing,
        isEventLogCategoryDeleting: store.eventLogCategories.isDeleting,
        eventLogCategoriesPosting: store.eventLogCategories.posting,

        expenseCategories: store.expenseCategories.expenseCategories,
        expenseCategoriesLoading: store.expenseCategories.loading,         
        currentExpenseCategory: store.expenseCategories.current,
        isExpenseCategoryAdding: store.expenseCategories.isAdding,
        isExpenseCategoryEditing: store.expenseCategories.isEditing,
        isExpenseCategoryDeleting: store.expenseCategories.isDeleting,
        expenseCategoriesPosting: store.expenseCategories.posting,

        needToUpdateEventLogCategories: store.eventLogCategories.needToUpdate,
        needToUpdateExpenseCategories: store.expenseCategories.needToUpdate
    }
}

export const mapDispatchToProps = {
    getEventLogCategories: () => getEventLogCategoriesAsyncThunk(),
    addingEventLogCategory: () => addingEventLogCategory(),
    editingEventLogCategory: (id: string) => editingEventLogCategoryAsyncThunk(id),
    postEventLogCategory: (eventLogCategory: EventLogCategory) => postEventLogCategoryAsyncThunk(eventLogCategory),
    putEventLogCategory: (eventLogCategory: EventLogCategory) => putEventLogCategoryAsyncThunk(eventLogCategory),
    clearEventLogCategory: () => clearEventLogCategory(),
    deletingEventLogCategory: (id: string) => deletingEventLogCategoryAsyncThunk(id),
    deleteEventLogCategory: (id: string) => deleteEventLogCategoryAsyncThunk(id),

    getExpenseCategories: () => getExpenseCategoriesAsyncThunk(),
    addingExpenseCategory: () => addingExpenseCategory(),
    editingExpenseCategory: (id: string) => editingExpenseCategoryAsyncThunk(id),
    postExpenseCategory: (expenseCategory: ExpenseCategory) => postExpenseCategoryAsyncThunk(expenseCategory),
    putExpenseCategory: (expenseCategory: ExpenseCategory) => putExpenseCategoryAsyncThunk(expenseCategory),
    clearExpenseCategory: () => clearExpenseCategory(),
    deletingExpenseCategory: (id: string) => deletingExpenseCategoryAsyncThunk(id),
    deleteExpenseCategory: (id: string) => deleteExpenseCategoryAsyncThunk(id)
}