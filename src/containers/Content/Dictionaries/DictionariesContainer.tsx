import React, { FC, useEffect } from 'react';
import { PrimaryButton, Label } from '@fluentui/react';
import EventLogCategoriesListComponent from '../../../components/Dictionaries/EventLogCategoriesListComponent';
import ExpensesCategoriesListComponent from '../../../components/Dictionaries/ExpenseCategoriesListComponent';
import ContentContainer from '../../ContentContainer/ContentContainer';
import ExpenseCategoryEditComponent from '../../../components/Dictionaries/ExpenseCategoryEditComponent';
import EventLogCategoryEditComponent from '../../../components/Dictionaries/EventLogCategoryEditComponent';
import DeleteDialog from '../../../components/DeleteDialog';

import { connect } from 'react-redux';
import { IDictionariesProps, mapStateToProps, mapDispatchToProps } from './IDictionariesProps';

const DictionariesContainer: FC<IDictionariesProps> = (props: IDictionariesProps) => {

    useEffect(() => {      
        
        if(props.needToUpdateEventLogCategories)
            props.getEventLogCategories();
            
    }, [props.getEventLogCategories, props.needToUpdateEventLogCategories]);

    useEffect(() => {  
        
        if(props.needToUpdateExpenseCategories)
            props.getExpenseCategories();      

    }, [props.getExpenseCategories, props.needToUpdateExpenseCategories]);

    const eventCategoryId = props.currentEventLogCategory ? props.currentEventLogCategory.id : '';
    const expenseCategoryId = props.currentExpenseCategory ? props.currentExpenseCategory.id : '';
    
    return (
        <>
            {/* EventLogCategories */}
            <ContentContainer 
                title="Изменения в рабочем календаре"
                showContent={props.expenseCategories!==null && props.eventLogCategories!==null}
            >                
                <PrimaryButton text="Добавить" onClick={()=>props.addingEventLogCategory()} className="mt-20"/>

                <EventLogCategoriesListComponent 
                    data={props.eventLogCategories || []}  
                    editingEventLogCategory={props.editingEventLogCategory} 
                    isLoading={props.eventLogCategoriesLoading}
                    deletingEventLogCategory={props.deletingEventLogCategory}
                />

                {!props.eventLogCategoriesLoading && props.eventLogCategories?.length===0 && 
                <Label className="text-center">
                    Нет данных для отображения
                </Label>}

                {(props.isEventLogCategoryAdding || props.isEventLogCategoryEditing) && props.currentEventLogCategory &&
                <EventLogCategoryEditComponent 
                    eventLogCategory={props.currentEventLogCategory} 
                    saveEventLogCategory={props.isEventLogCategoryAdding ? props.postEventLogCategory : props.putEventLogCategory} 
                    clearEventLogCategory={props.clearEventLogCategory}
                    posting={props.eventLogCategoriesPosting} 
                />}

                {props.isEventLogCategoryDeleting && props.currentEventLogCategory &&
                <DeleteDialog
                    hidden={!props.isEventLogCategoryDeleting}
                    deleteMethod={()=>props.deleteEventLogCategory(eventCategoryId)}
                    closeMethod={()=>props.clearEventLogCategory()}
                />}           
            </ContentContainer>    
        
            {/* ExpenseCategories */}
            <ContentContainer 
                title="Категории расходов"
                showContent={props.expenseCategories!==null && props.eventLogCategories!==null}
            >          
                <PrimaryButton text="Добавить" onClick={()=>props.addingExpenseCategory()} className="mt-20"/>

                <ExpensesCategoriesListComponent 
                    data={props.expenseCategories || []} 
                    editingExpenseCategory={props.editingExpenseCategory} 
                    isLoading={props.expenseCategoriesLoading}
                    deletingExpenseCategory={props.deletingExpenseCategory}
                />

                {!props.expenseCategoriesLoading && props.expenseCategories?.length===0 && 
                <Label className="text-center">
                    Нет данных для отображения
                </Label>}

                {(props.isExpenseCategoryAdding || props.isExpenseCategoryEditing) && props.currentExpenseCategory &&
                <ExpenseCategoryEditComponent 
                    expenseCategory={props.currentExpenseCategory} 
                    saveExpenseCategory={props.isExpenseCategoryAdding ? props.postExpenseCategory : props.putExpenseCategory} 
                    clearExpenseCategory={props.clearExpenseCategory}
                    posting={props.expenseCategoriesPosting}
                />}

                {props.isExpenseCategoryDeleting && props.currentExpenseCategory &&
                <DeleteDialog
                    hidden={!props.isExpenseCategoryDeleting}
                    deleteMethod={()=>props.deleteExpenseCategory(expenseCategoryId)}
                    closeMethod={()=>props.clearExpenseCategory()}
                />}                
            </ContentContainer>
        </>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(DictionariesContainer);