import React, { FC, useState, useEffect } from 'react';
import { PrimaryButton, Label, Stack, TextField, Image, ImageFit, KeyCodes } from '@fluentui/react';
import { useHistory } from 'react-router-dom';

import logo_light from '../../binwell_logo_light.png';
import logo_dark from '../../binwell_logo_dark.png';

import { requiredMessage } from '../../shared/Constants';
import { verticalGapStackTokens } from '../../shared/Styles';
import { ActionAsyncThunk } from '../../shared/Common';

interface Props{
    hasCredentials: boolean;
    login: (username: string, password: string) => ActionAsyncThunk;
    invertedTheme: boolean;
}

interface validationState{
    isValidUsername: boolean;
    isValidPassword: boolean;
}

const LoginFormComponent:FC<Props> = (props: Props) =>{

    const history = useHistory();

    // Set initial values
    const [username, setUsername] = useState<string | undefined>(undefined);
    const [password, setPassword] = useState<string | undefined>(undefined);
    const [showValidationError, setShowValidationError] = useState(false);

    const [validation, setValidation] = useState<validationState>({
        isValidUsername: false,
        isValidPassword: false
    });

    // Check credentials
    useEffect(()=>{
        if(props.hasCredentials){            
           history.push("/Calendar")
        }            
    }, [props.hasCredentials, history]);

    const _onChangeUsername = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, newValue?: string) => {        
        setUsername(newValue);
        setValidation({...validation, isValidUsername: newValue?.length!==0})
    }

    const _onChangePassword = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, newValue?: string) => {        
        setPassword(newValue);
        setValidation({...validation, isValidPassword: newValue?.length!==0})
    }

    const _onSubmit = () =>{
        if((validation.isValidUsername&&validation.isValidPassword)){
            props.login(username!.trim(), password!); 
            setShowValidationError(false);
        }
        else{
            setShowValidationError(true);
        }
    }

    const _onKeyPress = (event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>) =>
    {
        // Submit on Enter key
        if(event.charCode===KeyCodes.enter){
            _onSubmit()
        }
        // Block Space key
        if(event.charCode===KeyCodes.space){
            event.preventDefault()
        }
    }

    return(
        <Stack className="login-main-stack" tokens={verticalGapStackTokens}>
            <Image 
                src={props.invertedTheme ? logo_dark : logo_light} 
                height="45px" 
                imageFit={ImageFit.centerContain} 
                alt="Binwell logo"
            />
            <Label className="login-header">
                Добро пожаловать в Binwell Management System
            </Label>
            <TextField 
                label="Email" 
                required 
                value={username} 
                onChange={_onChangeUsername} 
                onKeyPress={_onKeyPress}
                errorMessage={showValidationError&&!validation.isValidUsername ? requiredMessage : undefined}
            />
            <TextField 
                label="Пароль" 
                required 
                type="password" 
                value={password} 
                onChange={_onChangePassword}
                onKeyPress={_onKeyPress}
                errorMessage={showValidationError&&!validation.isValidPassword ? requiredMessage : undefined}
            />
            <PrimaryButton 
                text="Войти" 
                onClick={()=>_onSubmit()} 
            />
        </Stack>
    )
}

export default LoginFormComponent;