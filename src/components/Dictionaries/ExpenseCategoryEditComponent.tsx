import React, { FC, useState } from 'react';
import { Stack, TextField } from '@fluentui/react';
import { ExpenseCategory } from '../../DAL/Dictionaries';
import { requiredMessage } from '../../shared/Constants';
import { RgbReg } from '../../shared/RegExpressions';
import { verticalGapStackTokens } from '../../shared/Styles';
import EditDialog from '../EditDialog';
import { ActionAsyncThunk } from '../../shared/Common';

interface Props{
    expenseCategory: ExpenseCategory;
    posting: boolean;
    saveExpenseCategory: (expenseCategory: ExpenseCategory) => ActionAsyncThunk<boolean, ExpenseCategory>;
    clearExpenseCategory: () => void;    
}

interface validationState{
    isValidTitle: boolean;
    isValidColor: boolean;
}

const ExpenseCategoryEditComponent: FC<Props> = (props: Props) => {

    // Set initial values
    const[expenseCategoryTitle, setExpenseCategoryTitle] = useState<string | undefined>(props.expenseCategory.title);
    const[expenseCategoryColor, setExpenseCategoryColor] = useState<string | undefined>(props.expenseCategory.color);

    const [validation, setValidation] = useState<validationState>({
        isValidTitle: props.expenseCategory.title ? props.expenseCategory.title.trim().length!==0 : false, 
        isValidColor: props.expenseCategory.color ? RgbReg.test(props.expenseCategory.color) : false
    });

    const _onCloseDialog = () => {
        props.clearExpenseCategory();
    }

    const _onSave = () => {
        const newExpenseCategory: ExpenseCategory = {
            id: props.expenseCategory.id,
            title: expenseCategoryTitle!.trim(),
            color: expenseCategoryColor!.trim().toUpperCase()
        }
        props.saveExpenseCategory(newExpenseCategory);
    }

    const _onChangeTitle = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, newValue?: string) => {        
        setExpenseCategoryTitle(newValue);
        if(newValue){
            setValidation({...validation, isValidTitle: newValue.trim().length!==0});
        } 
        else{
            setValidation({...validation, isValidTitle: false});
        }       
    }

    const _onChangeColor = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, newValue?: string) => {        
        setExpenseCategoryColor(newValue);
        if(newValue){
            setValidation({...validation, isValidColor: RgbReg.test(newValue)});
        }
        else{
            setValidation({...validation, isValidColor: false});
        }
    }

    const isValidForm = (
        validation.isValidTitle&&validation.isValidColor
    )

    return(
        <EditDialog 
            hidden={false}
            disabledSaveBtn={!isValidForm}
            saveMethod={()=>_onSave()}
            closeMethod={()=>_onCloseDialog()}
            posting={props.posting}
        >
            <Stack tokens={verticalGapStackTokens}>
                <TextField label="Наименование" 
                    required
                    value={expenseCategoryTitle} 
                    onChange={_onChangeTitle}                     
                    errorMessage={validation.isValidTitle ? undefined : requiredMessage}
                />
                <TextField 
                    required
                    label="Цвет" 
                    value={expenseCategoryColor} 
                    onChange={_onChangeColor}                     
                    errorMessage={validation.isValidColor ? undefined : requiredMessage}
                />
            </Stack>                
        </EditDialog>        
    )
}

export default ExpenseCategoryEditComponent;