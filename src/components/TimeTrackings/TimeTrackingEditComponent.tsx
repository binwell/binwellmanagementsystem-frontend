import React, { FC, useEffect, useState } from 'react';
import { Stack, TextField, ComboBox, IComboBoxOption, IComboBox, DatePicker, DayOfWeek } from '@fluentui/react';
import { WorkTask, TimeTrackingEdit } from '../../DAL/TimeTracking';
import { toUTC, getDateFromLocaleString, DAY_PICKER_STRINGS } from '../../shared/DateUtils';
import { requiredMessage } from '../../shared/Constants';
import { verticalGapStackTokens } from '../../shared/Styles';
import EditDialog from '../EditDialog';

interface Props{
    timeTracking: TimeTrackingEdit;
    workTasks: WorkTask[];
    posting: boolean;
    // saveTimeTracking: (timeTracking: TimeTrackingEdit | null) => Promise<void>;
    saveTimeTracking: (timeTracking: TimeTrackingEdit) => Promise<void>;
    clearTimeTracking: () => void;    
}

interface validationState{
    isValidWorkTaskId: boolean;
    isValidTimeSpent: boolean;
}

const TimeTrackingEditComponent: FC<Props> = (props: Props) => {
    
    // Set initial values
    const [timeTrackingWorkTaskId, setTimeTrackingWorkTaskId] = useState<string>(props.timeTracking.workTaskId);
    const [timeTrackingStartDate, setTimeTrackingStartDate] = useState<Date>(props.timeTracking.startDate);
    const [timeTrackingTimeSpent, setTimeTrackingTimeSpent] = useState<number>(props.timeTracking.timeSpent);

    const [WorkTasksOptions, setWorkTasksOptions] = useState<IComboBoxOption[]>();

    const [validation, setValidation] = useState<validationState>({
        isValidWorkTaskId: props.timeTracking.workTaskId !== '', 
        isValidTimeSpent: props.timeTracking.timeSpent!==0
    });
    
    // Set workTasks list options
    useEffect(()=>{
        const options: IComboBoxOption[] = props.workTasks.map(item=>{
            return{
                key: item.id,
                text: item.title
            } as IComboBoxOption
        });
        setWorkTasksOptions(options);                
    }, [props.workTasks]);

    const _onCloseDialog = () => {
        props.clearTimeTracking();
    }

    const _onSave = () => {
        const newTimeTracking: TimeTrackingEdit = {
            id: props.timeTracking.id,
            workTaskId: timeTrackingWorkTaskId,
            startDate: toUTC(timeTrackingStartDate),
            timeSpent: timeTrackingTimeSpent,
        }
        props.saveTimeTracking(newTimeTracking);
    }

    const _onChangeTask = (event: React.FormEvent<IComboBox>, option?: IComboBoxOption, index?: number, value?: string): void => {
        if(option){
            //setTimeTrackingWorkTaskId(Number.parseInt(option.key.toString()));
            setTimeTrackingWorkTaskId(option.key.toString());
            setValidation({...validation, isValidWorkTaskId: true})
        }
    }

    const _onChangeStartDate = (date: Date | null | undefined) => {      
        date ? setTimeTrackingStartDate(date) : setTimeTrackingStartDate(new Date());        
    }

    const _onChangeTimeSpent = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, newValue?: string) => {        
        setTimeTrackingTimeSpent(Number.parseInt(newValue!) || 0);
        setValidation({...validation, isValidTimeSpent: newValue?.toString()!=="0"})
    }

    const _onKeyPressTimeSpent = (event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        if(event.key<'0' || event.key>'9'){
            event.preventDefault()
        }
    }

    const isValidForm = (
        validation.isValidWorkTaskId&&validation.isValidTimeSpent
    )
    
    return(
        <EditDialog 
            hidden={false}
            disabledSaveBtn={!isValidForm}
            saveMethod={()=>_onSave()}
            closeMethod={()=>_onCloseDialog()}
            posting={props.posting}
        >
            <Stack tokens={verticalGapStackTokens}>                    
                <ComboBox 
                    required
                    label="Задача" 
                    options={WorkTasksOptions} 
                    onChange={_onChangeTask} 
                    selectedKey={timeTrackingWorkTaskId}                     
                    errorMessage={validation.isValidWorkTaskId ? undefined : requiredMessage}
                />
                <DatePicker 
                    label="Дата" 
                    firstDayOfWeek={DayOfWeek.Monday} 
                    value={timeTrackingStartDate} 
                    formatDate={(date?)=>date!.toLocaleDateString()} 
                    onSelectDate={_onChangeStartDate}
                    allowTextInput={true}
                    parseDateFromString={(string)=>getDateFromLocaleString(string)}
                    strings={
                        DAY_PICKER_STRINGS
                    }  
                />
                <TextField 
                    required 
                    label="Количество часов" 
                    value={timeTrackingTimeSpent?.toString()} 
                    onChange={_onChangeTimeSpent}
                    onKeyPress={_onKeyPressTimeSpent}                    
                    errorMessage={validation.isValidTimeSpent ? undefined : requiredMessage}
                />
            </Stack>
        </EditDialog>                     
    )
}

export default TimeTrackingEditComponent;