import React, { FC, useEffect, useState } from 'react';
import { Stack, TextField, ComboBox, IComboBoxOption, IComboBox } from '@fluentui/react';
import { WorkTask } from '../../DAL/TimeTracking';
import { Project } from '../../DAL/Projects';
import { requiredMessage } from '../../shared/Constants';
import { verticalGapStackTokens } from '../../shared/Styles';
import EditDialog from '../EditDialog';
import { ActionAsyncThunk } from '../../shared/Common';

interface Props{
    userId: string;
    projects: Project[];
    workTask: WorkTask;
    posting: boolean;
    saveWorkTask: (workTask: WorkTask) => ActionAsyncThunk<boolean, WorkTask>;
    clearWorkTask: () => void;
}

interface validationState{
    isValidTitle: boolean;
    isValidProject: boolean;
}

const WorkTaskEditComponent: FC<Props> = (props: Props) => {
    
    // Set initial values
    const [workTaskTitle, setWorkTaskTitle] = useState<string | undefined>(props.workTask.title);
    const [workTaskProjectId, setWorkTaskProjectId] = useState<string>(props.workTask.projectId);

    const [ProjectsOptions, setProjectsOptions] = useState<IComboBoxOption[]>();

    const [validation, setValidation] = useState<validationState>({
        isValidTitle: props.workTask.title ? props.workTask.title.trim().length!==0 : false, 
        isValidProject: props.workTask.projectId !== ''
    });
    
    // Set projects list options
    useEffect(()=>{
        const options: IComboBoxOption[] = props.projects?.map(item=>{
            return{
                key: item.id,
                text: item.title
            } as IComboBoxOption
        });
        setProjectsOptions(options);                
    }, [props.projects]);

    const _onCloseDialog = () => {
        props.clearWorkTask();
    }

    const _onSave = () => {
        const newWorkTask: WorkTask = {
            id: props.workTask.id,
            title: workTaskTitle!.trim(),
            projectId: workTaskProjectId,
            employeeId: props.userId,
        }
        props.saveWorkTask(newWorkTask);
    }

    const _onChangeTitle = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, newValue?: string) => {        
        setWorkTaskTitle(newValue);
        if(newValue){
            setValidation({...validation, isValidTitle: newValue.trim().length!==0})
        }
        else{
            setValidation({...validation, isValidTitle: false})
        }
    }

    const _onChangeWorkTaskProject = (event: React.FormEvent<IComboBox>, option?: IComboBoxOption, index?: number, value?: string): void => {
        if(option){
          //setWorkTaskProjectId(Number.parseInt(option.key.toString()));
          setWorkTaskProjectId(option.key.toString());
          setValidation({...validation, isValidProject: true})
        }
    }

    const isValidForm = (
        validation.isValidProject&&validation.isValidTitle
    )
    
    return(
        <EditDialog 
            hidden={false}
            disabledSaveBtn={!isValidForm}
            saveMethod={()=>_onSave()}
            closeMethod={()=>_onCloseDialog()}
            posting={props.posting}
        >
            <Stack tokens={verticalGapStackTokens}>
                <ComboBox 
                    required
                    label="Проект" 
                    options={ProjectsOptions} 
                    selectedKey={workTaskProjectId} 
                    onChange={_onChangeWorkTaskProject}                     
                    errorMessage={validation.isValidProject ? undefined : requiredMessage}
                />          
                <TextField 
                    required
                    label="Название" 
                    value={workTaskTitle} 
                    onChange={_onChangeTitle}                      
                    errorMessage={validation.isValidTitle ? undefined : requiredMessage}
                />
            </Stack>
        </EditDialog>                     
    )
}

export default WorkTaskEditComponent;