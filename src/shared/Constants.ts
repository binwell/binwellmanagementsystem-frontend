// Roles
export const EMPLOYEE = '87d31936-d2b5-426a-9459-685472c20974';
export const MANAGER = 'fb1bd8dc-b43d-4930-a2dc-0dc0e8be1e84';
export const ADMINISTRATOR = 'aa87b669-3df5-46f5-8997-ebc86e827c56';

// Approval statuses
export const CREATED = 'fe399ac8-63e5-460d-ba10-60948fb9fd27';
export const APPROVED = 'b4cec9af-9621-4e69-80b8-140770430616';
export const REJECTED = '31272170-0b02-430e-a4b3-489996ab055b';

// Error messages
export const requiredMessage = "Поле обязательно для заполнения";

export const zeroGuid = '00000000-0000-0000-0000-000000000000';