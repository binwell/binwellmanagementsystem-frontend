FROM node:lts as build-deps
WORKDIR /usr/src/app/
COPY package.json ./
RUN npm install
COPY . ./
RUN npm run build

FROM nginx:stable-perl
COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/conf.d/default.conf
ENV PORT 443
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]
